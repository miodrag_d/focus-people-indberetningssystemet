<?php

use app\app\controllers\dataGet;
use app\app\controllers\smsSender;
use Dotenv\Dotenv;
use Slim\App;

require '../vendor/autoload.php';
error_reporting(E_ERROR | E_PARSE);

$config['settings'] = ['displayErrorDetails' => true];

$dotenv = Dotenv::create(__DIR__.'/../');
$dotenv->load();

if($_SERVER['PHP_AUTH_USER'] != 'focusPeople' AND $_SERVER['PHP_AUTH_PW'] != 'kv8B5zTG7$O')
{
	header('WWW-Authenticate: Basic realm="focus"');
	header('HTTP/1.0 401 Unauthorized');
	echo 'NO GO!';
	exit;
}

require __DIR__.'/../app/app.php';

define('DIR_ROOT', __DIR__.'/..');
define('DIR_SRC', __DIR__.'/../src/');


$getData = new dataGet();
$getData->checkCoonection();
$getData->loadData();
$getData->processData();
$getData->newUserSMS();

if(date('H') == '23')
{
    ORM::raw_execute('
    UPDATE
    projects
    SET
    active = 2
    WHERE
    active = 1
    AND 
    DATE(last_update) != CURDATE()
    ');
}

/*
$masterData = [];
if (($handle = fopen('F:\Dropbox\code_sites\focus_new\temp\telefonNumre1578485738.csv', "r")) !== FALSE) {
	while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
		$masterData[$data[0]] = $data[1];
	}
	fclose($handle);
}

$tempFileData = file_get_contents('F:\Dropbox\code_sites\focus_new\temp\tempData.json');
$rawData = json_decode($tempFileData, true);

foreach ($rawData['projekter'] as $projekt)
{
	foreach ($projekt['medarbejder'] as $medarbejder)
	{
		$gemmelNummer = explode('/', $medarbejder['GamleNummer']);

		$mobilNummer = $masterData[trim($gemmelNummer[0])];
		$updateMedarbejder = ORM::for_table('user')
			->where('user_number', $medarbejder['NytNummer'])
			->find_one();

		if($updateMedarbejder)
		{
			$updateMedarbejder->set('old_number', $medarbejder['GamleNummer']);
			$updateMedarbejder->set('cell_number', $mobilNummer);
			$updateMedarbejder->save();
		}

	}
}
*/
/*
$newPasswords = ORM::for_table('user')
	->where('user_type', 1)
	->where_not_null('cell_number')
	->where_not_equal('name', 'MANGLER MANGLER')
	->find_many();

foreach ($newPasswords as $newPasswordRow)
{

	$newPass = rand(1111111,9999999);
	$newPasswordRow->set_expr('password', 'PASSWORD('.$newPass.')');
	$newPasswordRow->save();
	$cellNumber = $newPasswordRow['cell_number'];
	$smsTekst = 'Kære '.$newPasswordRow['name'].'. Vi er nu overgået til et nyt system og derfor sender vi dig denne sms. Når du fremover skal indberette dine timer, skal du forsat benytte http://tast.focus-people.dk . Dit nye medarbejder nummer er: '.$newPasswordRow['user_number'].' og din kode er: '.$newPass.' . Har du spørgsmål, er du velkommen til at kontakte os på telefon 22 333 112. Med venlig hilsen Focus People';
	$status = file_get_contents('https://api.sms1919.dk/rpc/push/?apikey='.$_ENV['SMS_API_KEY'].'&encoding=utf8&text='.urlencode($smsTekst).'&recipient=45'.$cellNumber);
	dump($smsTekst);
	dump($status);

	sleep(2);

}
*/
