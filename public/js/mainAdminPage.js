
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//$('#userTable').DataTable();

$('.logInAs').click(function () {
    var id = $(this).attr('userId');
    console.log(id);
    $.ajax
    ({
        type: "GET",
        url: "/api/getUserToken/" + id,
        dataType: 'json',
        complete : function(data, status) {
            console.log(data.responseJSON);
            if(data.responseJSON.token != '')
            {
                setCookie('focusPeople', data.responseJSON.token, 3);
                $(location).prop('href', '/');
            }

        }
    });

});

$('.fullList').click(function () {
    var id = $(this).attr('userId');
    var name = $(this).attr('name');

    $.ajax
    ({
        type: "GET",
        url: "/api/fullList/" + id,
        dataType: 'json',
        complete : function(data, status) {



            var newContent = '';

            newContent += '<table class="table table-striped">';
            newContent += '<thead>';
            newContent += '<tr>';
            newContent += '<th>Start</th>';
            newContent += '<th>Slut</th>';
            newContent += '<th>Time type</th>';
            newContent += '<th>Projekt nummer</th>';
            newContent += '<th>Projekt navn</th>';
            newContent += '<th></th>';
            newContent += '</tr>';
            newContent += '</thead>';

            if(data.responseJSON.list)
            {
                $.each(data.responseJSON.list, function (key, value) {
                    newContent += '<tr class="hourLine " doDelete="" workHourId="' + value.hourId + '">';
                    newContent += '<td>' + value.start_date + '</td>';
                    newContent += '<td>' + value.end_date + '</td>';
                    newContent += '<td>' + value.hour_type_name + '</td>';
                    newContent += '<td>' + value.number + '</td>';
                    newContent += '<td>' + value.name + '</td>';
                    newContent += '<td>';

                    if(value.locked != 1)
                    {
                        newContent += '<a href="#" class="btn btn-danger active deleteWorkHours" workHourId="' + value.hourId + '">Slet</a>';
                    }

                    newContent += '</td>';
                    newContent += '</tr>';
                });
            }

            newContent += '</table>';

            $('#mainModalXl .modal-header').html(name);
            $('#mainModalXl .modal-body').html(newContent);
            $('#mainModalXl').modal('show');

            $('.deleteWorkHours').one('click', function () {
                $(this).hide();
                var workHourId = $(this).attr('workHourId');
                $('.hourLine[workHourId="' + workHourId + '"]').addClass('bg-danger text-white').attr('doDelete', 1);
                $('#doDelete').show();
            });

        }
    });

});

$('#doDelete').click(function () {
    $('#doDelete').hide();
    var postArray = [];
    $('.hourLine[doDelete="1"]').each(function () {
        $(this).hide();
        postArray.push($(this).attr('workHourId'));

    });

    var postData = {};
    postData.deleteWorkHours = postArray;

    $.ajax
    ({
        type: "POST",
        url: "/api/deleteManyWorkHours",
        dataType: 'json',
        data: postData,
        complete : function(data, status) {
            if(data.responseJSON.message)
            {
                alert(data.responseJSON.message);
            }
        }
    });

});

$('.newKode').click(function () {
    var id = $(this).attr('userId');
    var modalContent = '';

    modalContent += '<table class="table">';

    modalContent += '<tr>';
    modalContent += '<td>Nulstil kode nu og få den vist</td>';
    modalContent += '<td><button class="btn btn-danger passwordReset" resetType="show" userId="' + id + '">Nulstil</button></td>';
    modalContent += '</tr>';

    modalContent += '<tr>';
    modalContent += '<td>Nulstil kode nu og send via sms</td>';
    modalContent += '<td><button class="btn btn-danger passwordReset" resetType="sms" userId="' + id + '">Nulstil</button></td>';
    modalContent += '</tr>';

    modalContent += '<tr>';
    modalContent += '<td>Besked</td>';
    modalContent += '<td id="message"></td>';
    modalContent += '</tr>';

    modalContent += '</table>';

    $('.modal-body').html(modalContent);
    $('#mainModal').modal('show');

    $('.passwordReset').one('click', function () {
        var postData = {};
        postData.userId = $(this).attr('userId');
        postData.resetType = $(this).attr('resetType');

        $.ajax
        ({
            type: "POST",
            url: "/api/resetpassword",
            dataType: 'json',
            data: postData,
            complete : function(data, status) {
                console.log(data.responseJSON.message);
                if(data.responseJSON.message != '')
                {
                    $('#message').html(data.responseJSON.message);
                }
            }
        });

    });
});