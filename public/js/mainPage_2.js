$(document).ready(function(){

    $('.approvWorker').click(function () {
       var workerId = $(this).attr('workerid');

        $.ajax
        ({
            type: "GET",
            url: "/api/approveWorker/" + workerId,
            dataType: 'json',
            complete : function(data, status) {
                console.log(data.responseJSON);

                if(data.responseJSON.status == true)
                {
                    location.reload();
                }
            }
        });
    });

    $('.hourInfo').click(function () {
        var workerId = $(this).attr('workerid');

        $.ajax
        ({
            type: "GET",
            url: "/api/workerInfo/" + workerId,
            dataType: 'json',
            complete : function(data, status) {
                $('#mainModal .modal-header').html('Periode skift');
                $('#mainModal .modal-body').html(data.responseJSON.content);
                $('#mainModal').modal('show');
            }
        });
    });


    $('.removeApproval').click(function () {
        $.ajax
        ({
            type: "GET",
            url: "/api/removeapproval",
            dataType: 'json',
            complete : function(data, status) {
                location.reload();
            }
        });
    });

    $('.removeApprovWorker').click(function () {
        var workerId = $(this).attr('workerid');

        $.ajax
        ({
            type: "GET",
            url: "/api/removeapprovalworker/" + workerId,
            dataType: 'json',
            complete : function(data, status) {
                location.reload();
            }
        });
    });

});