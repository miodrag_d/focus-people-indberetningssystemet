function pShift()
{
    $('.shiftPeriod').click(function () {
       var pId = $(this).attr('pid');

        $.ajax
        ({
            type: "GET",
            url: "/api/shift/" + pId,
            dataType: 'json',
            complete : function(data, status) {
                location.reload();
            }
        });
    });
}

function projektShift()
{
    $('.skiftProjectNow').click(function () {
        var projectindex = $(this).attr('projectindex');

        $.ajax
        ({
            type: "GET",
            url: "/api/shiftProject/" + projectindex,
            dataType: 'json',
            complete : function(data, status) {
                location.reload();
            }
        });
    });
}

$(document).ready(function(){
    $('#periodeShift').click(function () {

        $.ajax
        ({
            type: "GET",
            url: "/api/periodeShift",
            dataType: 'json',
            complete : function(data, status) {
                $('#mainModal .modal-header').html('Periode skift');
                $('#mainModal .modal-body').html(data.responseJSON.content);
                $('#mainModal').modal('show');
                pShift();
            }
        });
    });

    $('#swProject').click(function () {
        console.log('swProjekt');
        $.ajax
        ({
            type: "GET",
            url: "/api/projectShift",
            dataType: 'json',
            complete : function(data, status) {
                $('#mainModal .modal-header').html('Skift projekt');
                $('#mainModal .modal-body').html(data.responseJSON.content);
                $('#mainModal').modal('show');
                projektShift();
            }
        });
    });
});