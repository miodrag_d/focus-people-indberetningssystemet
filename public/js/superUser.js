
$('.superSelect').change(function () {
    var userId = $(this).attr('userId');
    var status = 'off';
    if($(this).is(':checked'))
    {
        status = 'on';
    }

    $.ajax
    ({
        type: "GET",
        url: "/api/savesuperuser/" + userId + "/" + status,
        dataType: 'json',
        complete : function(data, status) {

        }
    });

});