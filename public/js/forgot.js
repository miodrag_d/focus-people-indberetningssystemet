
$(document).ready(function(){

    //$('#mobileNumber')

    // Block non-numeric chars.
    $(document).on('keypress', 'input[type="number"]', function (event) {
        return ((event.which > 47) && (event.which < 58) || (event.which == 13));
    });

    $('#mobileNumber').focus();

    $('#send').click(function () {
        var postData = {};
        postData.cellNumber = $('#mobileNumber').val();

        $.ajax
        ({
            type: "POST",
            url: "/auth/newPassword",
            dataType: 'json',
            data: postData,
            complete : function(data, status) {
                console.log(data.responseJSON);

                if(data.responseJSON.type == 'bad')
                {
                    iziToast.warning({
                        title: 'Fejl',
                        message: data.responseJSON.message
                    });
                }

                if(data.responseJSON.type == true)
                {
                    iziToast.success({
                        title: 'Super godt!',
                        message: data.responseJSON.message,
                        onClosing: function(instance, toast, closedBy){
                            $(location).prop('href', '/login');
                        }
                    });
                }


            }
        });
    });
});