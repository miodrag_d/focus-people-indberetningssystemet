
$('#topInfoBar').hide();

$('.filterSelect').chosen({
    search_contains: true,
    width: "250px"
});

$('.dateRange').daterangepicker({autoUpdateInput: false});

$('.dateRange').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
});

$('.dateRange').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
});

$('.editorFilter').change(function () {

    if($('#projektSelect').val() > 0)
    {
        loadHourTypes($('#projektSelect').val());
    }

    getDataList();
});

$('.dateFilter').on('apply.daterangepicker', function(ev, picker) {
    getDataList();
});


function loadHourTypes(projectId) {
    $.ajax
    ({
        type: "GET",
        url: "/editorapi/getHourTypes/" + projectId + "?projectType=go",
        complete : function(data, status) {
            if(data.responseJSON.data.length > 0)
            {
                var selectOptions = '<option></option>';
                $.each(data.responseJSON.data, function (key, value) {
                    selectOptions += '<option value="' + value.id + '">' + value.name + '</option>';
                })

                $('#actionSelect').append('<option value="a6">Skift vagt typer</option>');
            }
            $('#hourTypeSelect').html(selectOptions).trigger('chosen:updated');
        }
    });
}

function getDataList() {
    $('#hourList').html($('#tableHeader').val());
    $('#selectAll').show();
    var postData = {};
    postData.projekt = $('#projektSelect').val();
    postData.user = $('#userSelect').val();
    postData.periode = $('#periodeSelect').val();
    postData.hourType = $('#hourTypeSelect').val();
    postData.vagtStart = $('#vagtStart').val();
    postData.vagtEnd = $('#vagtSlut').val();
    postData.approvels = $('#approvels').val();
    postData.listSorting = $('#listSorting').val();

    $.ajax
    ({
        type: "POST",
        url: "/api/getEditorList",
        dataType: 'json',
        data: postData,
        complete : function(data, status) {
           if(data.responseJSON.data.length > 0)
           {
               var tableLine = '';

                   $.each(data.responseJSON.data, function (key, value) {
                    console.log(value);

                    var logIcon = '';
                    if(value.parent_id > 0)
                    {
                        logIcon = '<i class="fas fa-list listLog" rowId="' + value.id + '"></i>';
                    }

                    tableLine += '<tr class="listTableRow" rowId="' + value.id + '" newHourType="" newStartDate="" newEndDate="" deleteRow="" newProjectId="" newUserId="" removeUserApprovel="" removeProjectApprovel="" setUserApprovel="" setProjectApprovel="">' +
                        '<td><input type="checkbox" class="lineSelect" rowId="' + value.id + '"></td>' +
                        '<td>' + value.periode_id + '</td>' +
                        '<td>(' + value.user_number + ') ' + value.user_name + '</td>' +
                        '<td>(' + value.project_number + ') ' + value.project_name + '</td>' +
                        '<td><input class="listDate listDateStart" rowId="' + value.id + '" returnNewValue="newStartDate" value="' + value.start_date.substring(0,16) + '" style="font-size: 10px; border: 0px;"></td>' +
                        '<td><input class="listDate listDateEnd" rowId="' + value.id + '" returnNewValue="newEndDate" value="' + value.end_date.substring(0,16) + '" style="font-size: 10px; border: 0px;"></td>' +
                        '<td style="cursor: pointer;" class="hourTypeCol">(' + value.hour_type_number + ') ' + value.hour_type_name + ' <i class="fas fa-pen"></i></td>' +
                        '<td>' + value.created_at.substring(0,16) + ' <i class="fas fa-info-circle" title="' + value.create_user_number + ' - ' + value.create_user_name + '"></i> ' + logIcon + '</td>';

                       tableLine += '<td>';
                       if(value.user_approved_user_id > 0)
                       {
                           tableLine += '<i class="fas fa-check-circle" style="color: #1c7430;" title="(' + value.user_approv_user_number + ') ' + value.user_approv_user_name + '\n'+ value.user_approved_at +'"></i>';
                       }
                       tableLine += '</td>';

                       tableLine += '<td>';
                       if(value.project_approved_user_id > 0)
                       {
                           tableLine += '<i class="fas fa-check-circle" style="color: #1c7430;" title="(' + value.project_approv_user_number + ') ' + value.project_approv_user_name + '\n'+ value.project_approved_at +'"></i>';
                       }
                       tableLine += '</td>';

                    tableLine += '<td class="lineAmount" align="right">' + value.hoursAmount + '</td>' +
                        '</tr>';
               });

               $('#hourList').append(tableLine);

               var totalAmout = 0;
               $('.lineAmount').each(function () {
                    totalAmout += parseFloat($(this).html());
               });

               var tableLineTotals = '';
               $('.lineSelect').change(function () {
                   var totalSelect = 0;
                    $('.lineSelect').each(function () {
                        var rowSelect = $(this).parent('td').parent('tr');
                        rowSelect.removeClass('listSelectShow');
                        if($(this).is(':checked'))
                        {
                            rowSelect.addClass('listSelectShow');
                            totalSelect += parseFloat(rowSelect.find('.lineAmount').html());
                            $('#hourListActions').show();
                            $('#actionTable').show();
                        }
                    });

                    $('#selectTotalRow').remove();
                   if(totalSelect > 0)
                   {
                       $('#hourList').append('<tr id="selectTotalRow">' +
                           '<td colspan="10">Total valgte</td>' +
                           '<td align="right">' + totalSelect.toFixed(2) + '</td>' +
                           '</tr>');
                   }
               });

               console.log(totalAmout);

               tableLineTotals += '<tr id="totalRow">' +
                   '<td colspan="10">Total</td>' +
                   '<td align="right">' + totalAmout.toFixed(2) + '</td>' +
                   '</tr>';

               $('#hourList').append(tableLineTotals);

               $('.listDate').daterangepicker({
                   "singleDatePicker": true,
                   "timePicker": true,
                   "timePicker24Hour": true,
                   "timePickerIncrement": 15,
                   "autoApply": true,
                   locale: {
                       format: 'YYYY-MM-DD HH:mm'
                   }
               }, function(start, end, label) {
                   console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
               });

               $('#selectAll').click(function () {
                   $('.lineSelect').prop('checked', true).change();
               });

               $('.listDate').change(function () {
                   var rowObj = $(this).parent('td').parent('tr');
                   var rowId = $(this).attr('rowId');
                   var returnNewValue = $(this).attr('returnNewValue');
                   var dateValue = $(this).val();
                   $('.lineSelect[rowId="' + rowId + '"]').prop('checked', true).change();
                   $('#actionSelect').val('line').change();

                   var postData = {};
                   postData.startDate = $('.listDateStart[rowId="' + rowId + '"]').val();
                   postData.startEnd = $('.listDateEnd[rowId="' + rowId + '"]').val();
                   postData.rowId = rowId;

                   $.ajax
                   ({
                       url: '/editorapi/checkHourEdit',
                       type: 'POST',
                       dataType: 'json',
                       data: postData,
                       complete : function(dataBob, status) {
                           if(dataBob.responseJSON.message != '')
                           {
                               $('#actionTable').hide();
                               alert(dataBob.responseJSON.message);
                               rowObj.addClass('listSelectAlert');
                               rowObj.attr(returnNewValue, '');
                           }
                           else
                           {
                               rowObj.attr(returnNewValue, dateValue);
                               rowObj.removeClass('listSelectAlert');
                               $('#actionTable').show();
                           }
                       }
                   });
               });

               $('.hourTypeCol').click(function () {
                    var rowId = $(this).parent('tr').attr('rowId');

                   $.ajax
                   ({
                       type: "GET",
                       url: "/editorapi/getHourTypes/" + rowId,
                       complete : function(data, status) {
                           changeHourType(rowId, data);
                       }
                   });
               });

               $('.sortClick').click(function () {
                   var sorttype = $(this).attr('sorttype');
                   console.log(sorttype);
                   $('#listSorting').val(sorttype).trigger('chosen:updated').change();
               });

               $('.listLog').click(function () {
                    var rowId = $(this).attr('rowId');

                   $.ajax
                   ({
                       type: "GET",
                       url: "/editorapi/workHourRowLog/" + rowId,
                       complete : function(data, status) {
                           if(data.responseJSON.logList.length > 0)
                           {
                               var modalContent = '';

                               $.each(data.responseJSON.logList, function (key, value) {
                                   modalContent += '<table class="table table-bordered">';
                                   modalContent += '<tr><td>Oprettet</td><td>' + value.created_at + '</td></tr>';
                                   modalContent += '<tr><td>Oprettet af</td><td>' + value.ca_user_number + ' ' + value.ca_user_name + '</td></tr>';
                                   modalContent += '<tr><td>Projekt</td><td>' + value.project_number + ' ' + value.project_name + '</td></tr>';
                                   modalContent += '<tr><td>Medarbejder</td><td>' + value.user_number + ' ' + value.user_name + '</td></tr>';
                                   modalContent += '<tr><td>Vagt type</td><td>' + value.hour_type_number + ' ' + value.hour_type_name + '</td></tr>';
                                   modalContent += '<tr><td>Start</td><td>' + value.start_date + '</td></tr>';
                                   modalContent += '<tr><td>Slut</td><td>' + value.end_date + '</td></tr>';
                                   modalContent += '</table>';
                               });

                               $('.modal-body').html(modalContent);
                               $('.modal-title').html('Linje log');
                               $('#mainModal').modal('show');

                           }
                       }
                   });
               });
           }
        }
    });
}

function setValueOnSelectedRows(attrName, value)
{
    console.log(attrName);
    console.log(value);
    $('.listSelectShow').attr(attrName, value);
}


$('#actionSelect').change(function () {
   var actionType = $(this).val();
   console.log(actionType);

   if(actionType == 'a1')
   {
       $('#actionSetting').html('Er du sikker? <a class="btn btn-success active" id="AreYouSureYes">Ja</a>  <a class="btn btn-danger active" id="AreYouSureNo">Afbryd</a>');

       $('#AreYouSureYes').click(function () {
           setValueOnSelectedRows('deleteRow', 1);
           $('#actionSetButton').show();
       });

       $('#AreYouSureNo').click(function () {
           getDataList();
       });
   }

    if(actionType == 'a2')
    {
        var projectOptions = $("#projektSelect").html();
        $('#actionSetting').html('<select id="moveToProjectSelect">' + projectOptions + '</select>');

        $('#moveToProjectSelect').change(function () {
            setValueOnSelectedRows('newProjectId', $('#moveToProjectSelect').val());
            $('#actionSetButton').show();
        });
    }

    if(actionType == 'a3')
    {
        var userOptions = $("#userSelect").html();
        $('#actionSetting').html('<select id="moveToUserSelect">' + userOptions + '</select>');

        $('#moveToUserSelect').change(function () {
            setValueOnSelectedRows('newUserId', $('#moveToUserSelect').val());
            $('#actionSetButton').show();
        });
    }

    if(actionType == 'a4')
    {
        setValueOnSelectedRows('removeUserApprovel', 1);
        $('#actionSetButton').show();
    }

    if(actionType == 'a5')
    {
        setValueOnSelectedRows('removeProjectApprovel', 1);
        $('#actionSetButton').show();
    }

    if(actionType == 'a6')
    {
        var hourTypeOptions = $("#hourTypeSelect").html();
        $('#actionSetting').html('<select id="moveToHourTypeSelect">' + hourTypeOptions + '</select>');

        $('#moveToHourTypeSelect').change(function () {
            setValueOnSelectedRows('newHourType', $('#moveToHourTypeSelect').val());
            $('#actionSetButton').show();
        });
    }

    if(actionType == 'a7')
    {
        setValueOnSelectedRows('setUserApprovel', 1);
        $('#actionSetButton').show();
    }

    if(actionType == 'a8')
    {
        setValueOnSelectedRows('setProjectApprovel', 1);
        $('#actionSetButton').show();
    }

    if(actionType == 'line')
    {
        $('#actionSetButton').show();
    }
});

function changeHourType(rowId, data) {
    console.log(data.responseJSON);

    var modalContent = '';

    modalContent += '<table class="table">';

    $.each(data.responseJSON.data, function (key, value) {
        modalContent += '<tr>';
        modalContent += '<td>' + value.name + '</td>';
        modalContent += '<td width="100"><a class="btn btn-success active hourTypeSelect" typeId="' + value.id + '" newDisplayName="' + value.name + '">Vælg</a></td>';
        modalContent += '</tr>';
    });

    modalContent += '</table>';


    $('.modal-body').html(modalContent);
    $('.modal-title').html('Skift vagt type');
    $('#mainModal').modal('show');

    $('.hourTypeSelect').one('click', function () {
        var typeId = $(this).attr('typeId');
        var displayName = $(this).attr('newDisplayName');
        $('.listTableRow[rowId="' + rowId + '"]').attr('newHourType', typeId);
        $('.listTableRow[rowId="' + rowId + '"]').find('.hourTypeCol').html(displayName);
        $('.lineSelect[rowid="' + rowId + '"]').prop('checked', true).change();
        $('#actionSelect').val('line').change();
        $('#mainModal').modal('hide');
    });
}

function getLineData()
{
    var maxPostData = [];

    $('.listSelectShow').each(function () {
        var lineData = {};

        lineData.actionType = $('#actionSelect').val();
        lineData.rowId = $(this).attr('rowid');
        lineData.newHourType = $(this).attr('newHourType');
        lineData.newStartDate = $(this).attr('newStartDate');
        lineData.newEndDate = $(this).attr('newEndDate');
        lineData.deleteRow = $(this).attr('deleteRow');
        lineData.newProjectId = $(this).attr('newProjectId');
        lineData.newUserId = $(this).attr('newUserId');
        lineData.removeUserApprovel = $(this).attr('removeUserApprovel');
        lineData.removeProjectApprovel = $(this).attr('removeProjectApprovel');
        lineData.setUserApprovel = $(this).attr('setUserApprovel');
        lineData.setProjectApprovel = $(this).attr('setProjectApprovel');

        maxPostData.push(lineData);
    });

    return maxPostData;
}

$('#actionSetButton').click(function ()
{
    var postData = getLineData();
    console.log(postData);

    $.ajax
    ({
        url: '/editorapi/actionHandler',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(postData),
        complete : function(dataBob, status) {
            $('#hourListActions').hide();
            $('#actionTable').hide();
            $('#actionSetting').html('');
            getDataList();
        }
    });

});

