function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

$(document).ready(function(){

    $('#logIn').click(function () {
        var postData = {};
        postData.user = $('#user').val();
        postData.password =$('#password').val();

        $.ajax
        ({
            type: "POST",
            url: "/auth/login",
            dataType: 'json',
            data: postData,
            complete : function(data, status) {
                console.log(data.responseJSON);

                if(data.responseJSON.message != '' & data.responseJSON.token == '')
                {
                    iziToast.warning({
                        title: 'Fejl',
                        message: data.responseJSON.message
                    });
                }

                if(data.responseJSON.message == '' & data.responseJSON.token != '')
                {
                    setCookie('focusPeople', data.responseJSON.token, 3);
                    $(location).prop('href', '/');
                }

            }
        });
    });
});

var isChromium = window.chrome;
var winNav = window.navigator;
var vendorName = winNav.vendor;
var isOpera = typeof window.opr !== "undefined";
var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
var isIOSChrome = winNav.userAgent.match("CriOS");

if (isIOSChrome) {
    // is Google Chrome on IOS
} else if(
    isChromium !== null &&
    typeof isChromium !== "undefined" &&
    vendorName === "Google Inc." &&
    isOpera === false &&
    isIEedge === false
) {
    $('#browsermessage').html('');
} else {

}
