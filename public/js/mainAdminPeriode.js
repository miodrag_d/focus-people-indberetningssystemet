$('.lockPeriod').click(function () {
    var persiodeId = $(this).attr('pid');

    $.ajax
    ({
        type: "GET",
        url: "/api/lookperiode/" + persiodeId,
        dataType: 'json',
        complete : function(data, status) {
            $(location).prop('href', '/perioder');

        }
    });
});