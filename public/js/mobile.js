

$('#topInfoBar').hide();
$('#fpfooter').hide();

$('#startDate').change(function () {
    var startDate = $(this).val();
    $('#endDate').val(startDate);
});

$('.timeSelect').change(function () {

    var postData = {};
    postData.startDate = $('#startDate').val();
    postData.endDate = $('#endDate').val();
    postData.startTime = $('#startTime').val();
    postData.endTime = $('#endTime').val();

    $.ajax
    ({
        type: "POST",
        url: "/api/hourCheck",
        dataType: 'json',
        data: postData,
        complete : function(data, status) {
            if(data.responseJSON.hours > 0)
            {
                $('#saveRow').show();

                $('#showHours').show();
                $('#showHourNumber').html(data.responseJSON.hours);
            }
            console.log(data.responseJSON.hours);
        }
    });

});

$('.mobileDelete').click(function () {
    var workHourId = $(this).attr('workHourId');
    $(this).hide();

    $('.mobileDeleteConfirm[workHourId="' + workHourId + '"]').show();
});

$('.mobileDeleteConfirm').click(function () {
    var workHourId = $(this).attr('workHourId');
    $(this).parent('tr').hide();

    $.ajax
    ({
        type: "GET",
        url: "/api/deleteWorkHour/" + workHourId,
        dataType: 'json',
        complete : function(data, status) {
            console.log(data);

            if(data.responseJSON.status == true)
            {
                location.reload();
            }
        }
    });
});


$('#saveNow').click(function () {
    var postData = {};
    postData.startDate = $('#startDate').val();
    postData.endDate = $('#endDate').val();
    postData.startTime = $('#startTime').val();
    postData.endTime = $('#endTime').val();
    postData.hourTypeId = $('#hourTypeId').val();

    console.log(postData);

    $.ajax
    ({
        type: "POST",
        url: "/api/saveWorkHours",
        dataType: 'json',
        data: postData,
        complete : function(data, status) {
            if(data.responseJSON.message == true)
            {
                location.reload();
            }
            else
            {
                iziToast.warning({
                    title: 'Fejl',
                    message: data.responseJSON.message
                });
            }
        }
    });

});