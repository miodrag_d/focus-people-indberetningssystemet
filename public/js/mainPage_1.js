function moveEndDate() {

    if($('#start_date').attr('data-value') == $('#end_date').attr('data-value'))
    {
        var curentDate = $('#end_date').attr('data-value');
        var myDate = new Date(curentDate);
        myDate.setDate(myDate.getDate() + 1);
        var datestring =  myDate.getFullYear() +
            "-" + ("0"+(myDate.getMonth()+1)).slice(-2) +
            "-" + ("0" + myDate.getDate()).slice(-2);

        $('#end_date').attr('data-value', datestring);

        var displayDate = ("0" + myDate.getDate()).slice(-2) +
            "-" + ("0"+(myDate.getMonth()+1)).slice(-2) +
            "-" + myDate.getFullYear();

        $('#end_date').html(displayDate);
    }
}

function addOneDay()
{
    $('#addOneDay').click(function () {
        moveEndDate();
    });
}

function deleteWorkHour()
{
    $('#deleteWorkHours').click(function () {
        var curentId = $('#fromId').val();

        console.log(curentId);

        if(curentId != '')
        {
            $.ajax
            ({
                type: "GET",
                url: "/api/deleteWorkHour/" + curentId,
                dataType: 'json',
                complete : function(data, status) {
                    console.log(data);

                    if(data.responseJSON.status == true)
                    {
                        $('#workHourModal').modal('hide');
                        location.reload();
                    }
                }
            });
        }
    });
}

$(document).ready(function(){

    $('.workHours').click(function () {
        var editid = $(this).attr('editid');

        $('#fromId').val(editid);

        $('#deleteWorkHours').show();

        $.ajax
        ({
            type: "GET",
            url: "/api/getWorkHours/" + editid,
            dataType: 'json',
            complete : function(data, status) {
                console.log(data.responseJSON);

                if(data.responseJSON.data.id)
                {
                    $('#saveWorkHours').show();
                    if(data.responseJSON.data.project_approved_at != null)
                    {
                        $('#saveWorkHours').hide();
                        $('#deleteWorkHours').hide();
                    }

                    $('#start_date').attr('data-value', data.responseJSON.data.startDate).html(data.responseJSON.data.showStartDate);
                    $('#end_date').attr('data-value', data.responseJSON.data.endDate).html(data.responseJSON.data.showEndDate);

                    if(data.responseJSON.data.showStartDate == data.responseJSON.data.showEndDate)
                    {
                        $('#end_date').append(' <a class="btn btn-info btn-sm" id="addOneDay" style="color: #FFFFFF;">+ 1 dag</a>');
                        addOneDay();
                    }

                    $('#startTime').val(data.responseJSON.data.startTime).change();
                    $('#endTime').val(data.responseJSON.data.endTime).change();
                    $('#hourTypeId').val(data.responseJSON.data.hourTypeId).change();

                    deleteWorkHour()
                    $('#workHourModal').modal('show');
                }
            }
        });

        console.log(editid);
    });

    $('.dateRow').click(function () {
       var rowDate = $(this).attr('rowDate');
       var displayDate = $(this).attr('displayDate');

        $('#fromId').val('');

       $('#start_date').attr('data-value', rowDate).html(displayDate);
       $('#end_date').attr('data-value', rowDate).html(displayDate + ' <a class="btn btn-info btn-sm" id="addOneDay" style="color: #FFFFFF;">+ 1 dag</a>');

        $.ajax
        ({
            type: "GET",
            url: "/api/lastWorkHourType",
            dataType: 'json',
            complete : function(data, status) {
                console.log(data.responseJSON);

                if(data.responseJSON.hour_type_id)
                {
                    $('#hourTypeId').val(data.responseJSON.hour_type_id).change();
                }
            }
        });
        $('#deleteWorkHours').hide();
        $('#workHourModal').modal('show');
        addOneDay();
    });

    $('#workHourModal').on('shown.bs.modal', function () {
        $("#startTime").focus();
    });

    $('#startTime').change(function () {
        $('#kopiRow').hide();
    });

    $('#kopiFraDato').change(function () {
        $('.vagtRow').hide();

        $.ajax
        ({
            type: "GET",
            url: "/api/copyWorkDateList/" + $('#kopiFraDato').val(),
            dataType: 'json',
            complete : function(data, status) {
                console.log(data.responseJSON);
                var hoursTabel = '<table class="table">';
                if(data.responseJSON.hours.length > 0)
                {
                    $.each(data.responseJSON.hours, function (key, value) {
                        hoursTabel += '<tr>';
                        hoursTabel += '<td>' + value.start + '</td>';
                        hoursTabel += '<td>' + value.end + '</td>';
                        hoursTabel += '<td>' + value.hour_type_name + '</td>';
                        hoursTabel += '</tr>';
                    });
                }
                hoursTabel += '</table>';
                $('#kopiRow').after('<tr><td colspan="2">' + hoursTabel + '</td></tr>');
            }
        });

    });

    $('#endTime').blur(function () {
        var startTime = parseInt($('#startTime').val());
        var endTime = parseInt($('#endTime').val());

        if(endTime < startTime)
        {
            moveEndDate();
        }
    });

    $('#saveWorkHours').click(function () {
        var postData = {};
        postData.startDate = $('#start_date').attr('data-value');
        postData.endDate = $('#end_date').attr('data-value');
        postData.startTime = $('#startTime').val();
        postData.endTime = $('#endTime').val();
        postData.hourTypeId = $('#hourTypeId').val();
        postData.currentId = $('#fromId').val();
        postData.copyFromDate = $('#kopiFraDato').val();

        console.log(postData);

        $.ajax
        ({
            type: "POST",
            url: "/api/saveWorkHours",
            dataType: 'json',
            data: postData,
            complete : function(data, status) {
                if(data.responseJSON.message == true)
                {
                    $('#workHourModal').modal('hide');
                    location.reload();
                }
                else
                {
                    iziToast.warning({
                        title: 'Fejl',
                        message: data.responseJSON.message
                    });
                }
            }
        });

    });

    $('#approvHours').click(function () {
        $.ajax
        ({
            type: "GET",
            url: "/api/approvHours",
            dataType: 'json',
            complete : function(data, status) {
                if(data.responseJSON.status == true)
                {
                    location.reload();
                }
            }
        });
    });

    console.log(window.screen.availHeight);
    console.log(window.screen.availWidth);

    if(window.screen.availWidth < 500)
    {
        window.location.href = '/mobile';
    }

});