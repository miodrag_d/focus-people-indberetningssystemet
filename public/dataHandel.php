<?php

use app\app\controllers\dataGet;
use app\app\controllers\smsSender;
use Dotenv\Dotenv;
use Slim\App;

require '../vendor/autoload.php';
error_reporting(E_ERROR | E_PARSE);

$config['settings'] = ['displayErrorDetails' => true];

$dotenv = Dotenv::create(__DIR__.'/../');
$dotenv->load();

require __DIR__.'/../app/app.php';

define('DIR_ROOT', __DIR__.'/..');
define('DIR_SRC', __DIR__.'/../src/');
/*
$personCheck = [];
$finalFile = [];
$row = 1;
if (($handle = fopen("C:\Users\mail\Desktop\medarbejder_skattekort.csv", "r")) !== FALSE) {
	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

		if($row == 1)
		{
			$header = $data;
		}

		if(!in_array($data[1], $personCheck) AND $row > 1)
		{
			$finalFile[] = $data;
			$personCheck[] = $data[1];
			//dump($data);
		}

		$row ++;
	}
	fclose($handle);
}

$output = fopen('F:\Dropbox\code_sites\focus_new\temp\medarbejder_skattekort.csv','w') or die("Can't open php://output");
fputcsv($output, $header);
foreach($finalFile as $product) {
	fputcsv($output, $product);
}
*/

$bob = ORM::for_table('project_hour_types')
	->where('active', 1)
	->find_many();

foreach ($bob as $bobRow)
{
	if(substr($bobRow['name'],0,3) == 'ALM')
	{
		$bobRow->set('pre_select', 1);
		$bobRow->save();
	}
}