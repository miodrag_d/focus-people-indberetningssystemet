<?php

use Dotenv\Dotenv;
use Slim\App;

require '../vendor/autoload.php';

$config['settings'] = ['displayErrorDetails' => true];

$dotenv = Dotenv::create(__DIR__.'/../');
$dotenv->load();

$app = new App($config);

require __DIR__.'/../app/app.php';
require __DIR__.'/../app/routes.php';

define('DIR_ROOT', __DIR__.'/..');
define('DIR_SRC', __DIR__.'/../src/');

$app->run();