<html>
<head>
	<link rel="stylesheet" href="/css/login.css">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/lib/iziToast/dist/css/iziToast.css">
	<title>Sign in</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="/lib/iziToast/src/js/iziToast.js"></script>

</head>
<body>
<div class="main">
	<p class="sign" align="center"><img src="/images/logo.png" width="200"></p>
	<p id="browsermessage" align="center">Dette system er bygget og optimeret til Google Chrome. Vi anbefalder at du benytter denne. Den kan hentes gratis her. <a href="https://www.google.com/chrome/">https://www.google.com/chrome/</a></p>
	<form class="form1">
		<input class="un " type="text" id="user" align="center" placeholder="ID">
		<input class="pass" type="password" id="password" align="center" placeholder="Kode">
		<a class="submit" id="logIn" align="center">Log in</a>
		<p class="forgot" align="center"><a href="/fg">Glemt kode?</p>
</div>
</body>
<script src="/js/login.js"></script>
</html>