<html>

<head>
	<link rel="stylesheet" href="/css/login.css">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/lib/iziToast/dist/css/iziToast.css">
	<title>Sign in</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="/lib/iziToast/src/js/iziToast.js"></script>
</head>
<body>
<div class="main">
	<p class="sign" align="center"><img src="/images/logo.png" width="200"></p>
		<input class="un" type="number" id="mobileNumber" align="center" maxlength="8" placeholder="Mobil nummer">
		<a class="submit" id="send" align="center">Send</a>
        <p class="forgot" align="center"><a href="/login">Log ind</p>

</div>
</body>
<script src="/js/forgot.js"></script>
</html>