<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Focus People</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<link rel="stylesheet" href="/lib/iziToast/dist/css/iziToast.css">
	<link rel="stylesheet" href="/lib/chosen/chosen.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

	<!-- Custom styles for this template -->
	<link href="/css/shop-homepage.css" rel="stylesheet">
    <script src="/lib/iziToast/src/js/iziToast.js"></script>
    <script src="https://kit.fontawesome.com/cf85629014.js" crossorigin="anonymous"></script>


</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
		<a class="navbar-brand" href="/"><img src="/images/logo_small.png" width="200"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
                <?=$topMenu?>
				<li class="nav-item">
					<a class="nav-link" href="/logout">Log ud</a>
				</li>
			</ul>
		</div>
	</div>
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row" style="height: 10px;">
    </div>
    <div class="row" id="topInfoBar">
        <div class="col-4">Brugere: <b><?=$userName?></b></div>
        <div class="col-4" style="cursor: pointer;" id="swProject">Projekt: <b><?=$projektName?></b></div>
        <div class="col-4" id="periodeShift" style="cursor: pointer;">Periode: <b><?=$periode?></b> <i class="fa fa-calendar"></i><?=$returnBotton?></div>
    </div>
	<?=$content?>

</div>
<!-- /.container -->
<div id="mainModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">x</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Luk</button>
            </div>
        </div>

    </div>
</div>
<!-- Footer -->

<div id="mainModalXl" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">x</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="doDelete" style="display: none;">Udfør sletning</button>
				<button type="button" class="btn btn-info" data-dismiss="modal">Luk</button>
			</div>
		</div>

	</div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="/lib/vendor/jquery/jquery.min.js"></script>
<script src="/lib/jquery-cookie/jquery.cookie.js"></script>
<script src="/lib/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/lib/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script src="/js/siteGen.js"></script>

<?=$jsFiles?>

</body>

</html>
