<?php


namespace app\app\controllers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class adminPages extends mainController
{
    public function totalReport()
    {
        $main = '';
        $main .= '<table class="table table-bordered">';

        $status = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.periode_id', $this->periodeId)
            ->where('user_work_hours.active', 1)
            ->where('projects.active', 1)
            ->where_not_null('projects.name')
            ->join('user', 'user.id = user_work_hours.user_id AND user.internal_user IS NULL')
            ->join('projects', 'projects.id = user_work_hours.project_id')
            ->select_expr('SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60)', 'totalTimer')
            //->select_expr('(CASE WHEN user_approved_at IS NOT NULL THEN SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60) else 0 END)', 'totalTimerAp1')
            ->select_expr('SUM((CASE WHEN project_approved_at IS NOT NULL THEN (unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60 else 0 END))', 'totalHoursApproved')
            ->group_by('user_work_hours.user_id')
            ->group_by('user_work_hours.project_id')
            //->group_by('user_work_hours.periode_id')
            ->select('user.name', 'user_name')
            ->select('user.user_number', 'user_number')
            ->select('projects.id', 'projects_id')
            ->select('projects.name', 'projects_name')
            ->select('projects.number', 'projects_number')
            ->select('projects.bevilling_id', 'projects_bevilling_id');

        $displayLines = [];
        $projectTotal = [];
        $projectTotalApproved = [];
        $projectData = [];

        $monsterTotal = 0;
        $monsterTotalApproved = 0;


        foreach ($status->find_array() as $rapportRow)
        {
            $projectData[$rapportRow['projects_number']] = [
                'projects_number' => $rapportRow['projects_number'],
                'projects_name' => $rapportRow['projects_name'],
                'projects_id' => $rapportRow['projects_id']
            ];
            $displayLines[$rapportRow['projects_number']][] = [$rapportRow['user_number'],$rapportRow['user_name'],$rapportRow['totalTimer'],$rapportRow['totalHoursApproved']];
            $projectTotal[$rapportRow['projects_number']] = $projectTotal[$rapportRow['projects_number']] + $rapportRow['totalTimer'];
            $projectTotalApproved[$rapportRow['projects_number']] = $projectTotalApproved[$rapportRow['projects_number']] + $rapportRow['totalHoursApproved'];

            $monsterTotal = $monsterTotal + $rapportRow['totalTimer'];
            $monsterTotalApproved = $monsterTotalApproved + $rapportRow['totalHoursApproved'];
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Projekt nummer');
        $sheet->setCellValue('B1', 'Projekt');
        $sheet->setCellValue('C1', 'Medarbejder nummer');
        $sheet->setCellValue('D1', 'Medarbejder');
        $sheet->setCellValue('E1', 'Tastet');
        $sheet->setCellValue('F1', 'Godkendt');

        $row = 1;
        $calSum = 0;
        foreach ($projectData as $projectDataRow)
        {
            $main .= '<tr>';
            $main .= '<td style="font-weight: bold;">'.$projectDataRow['projects_number'].'</td>';
            $main .= '<td style="font-weight: bold;">'.$projectDataRow['projects_name'].'</td>';
            $main .= '<td></td>';
            $main .= '<td></td>';
            $main .= '<td>Tastet</td>';
            $main .= '<td>Godkendt</td>';
            $main .= '</tr>';

            $row ++;
            $sheet->setCellValueByColumnAndRow(1, $row, $projectDataRow['projects_number']);
            $sheet->setCellValueByColumnAndRow(2, $row, $projectDataRow['projects_name']);


            foreach ($displayLines[$projectDataRow['projects_number']] as $medarbejderRow)
            {

                $main .= '<tr>';
                $main .= '<td></td>';
                $main .= '<td></td>';
                $main .= '<td>'.$medarbejderRow[0].'</td>';
                $main .= '<td>'.$medarbejderRow[1].'</td>';
                $main .= '<td align="right">'.number_format($medarbejderRow[2],2).'</td>';
                $main .= '<td align="right">'.number_format($medarbejderRow[3],2).'</td>';
                $main .= '</tr>';

                $row ++;
                $sheet->setCellValueByColumnAndRow(3, $row, $medarbejderRow[0]);
                $sheet->setCellValueByColumnAndRow(4, $row, $medarbejderRow[1]);
                $sheet->setCellValueByColumnAndRow(5, $row, $medarbejderRow[2]);
                $sheet->setCellValueByColumnAndRow(6, $row, $medarbejderRow[3]);

            }

            $lineColor = '';

            if($projectTotal[$projectDataRow['projects_number']] != $projectTotalApproved[$projectDataRow['projects_number']])
            {
                $lineColor = 'style="background-color: #721c24; color: #ffffff;"';
            }

            $main .= '<tr '.$lineColor.'>';
            $main .= '<td></td>';
            $main .= '<td></td>';
            $main .= '<td></td>';
            $main .= '<td style="font-weight: bold;">TOTAL</td>';
            $main .= '<td align="right" style="font-weight: bold;">'.number_format($projectTotal[$projectDataRow['projects_number']],2).'</td>';
            $main .= '<td align="right" style="font-weight: bold;">'.number_format($projectTotalApproved[$projectDataRow['projects_number']],2).'</td>';
            $main .= '</tr>';

            $row ++;
            $sheet->setCellValueByColumnAndRow(4, $row, 'Total');
            $sheet->setCellValueByColumnAndRow(5, $row, $projectTotal[$projectDataRow['projects_number']]);
            $sheet->setCellValueByColumnAndRow(6, $row, $projectTotalApproved[$projectDataRow['projects_number']]);

        }

        $main .= '<tr>';
        $main .= '<td></td>';
        $main .= '<td></td>';
        $main .= '<td></td>';
        $main .= '<td style="font-weight: bold;">TOTAL FOR ALLE PROJEKTER</td>';
        $main .= '<td align="right" style="font-weight: bold;">'.number_format($monsterTotal,2).'</td>';
        $main .= '<td align="right" style="font-weight: bold;">'.number_format($monsterTotalApproved,2).'</td>';
        $main .= '</tr>';

        $main .= '</table>';

        $row ++;
        $sheet->setCellValueByColumnAndRow(4, $row, 'TOTAL FOR ALLE PROJEKTER');
        $sheet->setCellValueByColumnAndRow(5, $row, $monsterTotal);
        $sheet->setCellValueByColumnAndRow(6, $row, $monsterTotalApproved);

        if($_GET['excel'] == 'go')
        {
            $sheet->getColumnDimension('A')->setAutoSize(true);
            $sheet->getColumnDimension('B')->setAutoSize(true);
            $sheet->getColumnDimension('C')->setAutoSize(true);
            $sheet->getColumnDimension('D')->setAutoSize(true);

            $sheet->getStyle('E3:E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
            $sheet->getStyle('F3:F'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $writer = new Xlsx($spreadsheet);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Projekt_totaler_'.date('d-m-Y_H_i_s').'.xlsx"');
            $writer->save("php://output");

            exit;
        }

        $this->displayPage($main);
    }

    public function rapportList()
    {
        $main = '<h4>Rapporter</h4>';

        $main .= '<table class="table table-hover">';

        $main .= '<tr><td>Projekt totaler</td><td width="100" align="center"><a href="/admin/totalreport" class="btn btn-info">Åben</a></td><td width="100" align="center"><a href="/admin/totalreport?excel=go" class="btn btn-info">Excel</a></td></tr>';

        $main .= '</table>';


        $this->displayPage($main);
    }

    public function superUsers()
    {
        if($this->userType() != 5)
        {
            die;
        }

        $main = '<h4>Super users</h4>';

        $main .= '<table class="table table-hover">';

        foreach (\ORM::for_table('user')->where_not_null('internal_user')->find_array() as $userRow)
        {
            $main .= '<tr>';
            $main .= '<td>'.$userRow['user_number'].'</td>';
            $main .= '<td>'.$userRow['name'].'</td>';
            $main .= '<td>'.$userRow['cell_number'].'</td>';

            $checkSelect = '';
            if($userRow['user_type'] == 4)
            {
                $checkSelect = 'CHECKED';
            }

            $main .= '<td><input class="superSelect" userId="'.$userRow['id'].'" type="checkbox" value="1" '.$checkSelect.'></td>';

            $main .= '</tr>';
        }

        $main .= '</table>';

        $this->jsFiles[] = 'superUser.js';
        $this->displayPage($main);
    }

    public function editor()
    {
        if($this->userType() < 4)
        {
            die();
        }

        $main = '';

        //options
        $projects = '<option></option>';
        foreach (\ORM::for_table('projects')->order_by_asc('number')->find_array() as $projectRow)
        {
            $status = 'Aktiv';
            if($projectRow['active'] != 1)
            {
                $status = 'Inaktiv';
            }

            $projects .= '<option value="'.$projectRow['id'].'">'.$projectRow['number'].' - '.$projectRow['name'].' ('.$status.')</option>';
        }

        $users = '<option></option>';
        foreach (\ORM::for_table('user')->where('user_type', 1)->order_by_asc('user_number')->find_array() as $userRow)
        {
            $users .= '<option value="'.$userRow['id'].'">'.$userRow['user_number'].' - '.$userRow['name'].'</option>';
        }

        $perioder = '<option></option>';
        foreach (\ORM::for_table('periodes')->order_by_asc('start_date')->find_array() as $periodeRow)
        {
            $perioder .= '<option value="'.$periodeRow['id'].'">('.$periodeRow['id'].') '.date('d-m-Y', strtotime($periodeRow['start_date'])).' / '.date('d-m-Y', strtotime($periodeRow['end_date'])).'</option>';
        }


        $main .= '<table class="table table-borderless table-sm" >';

       $main .= '<tr>';
       $main .= '<td><h5>Filter</h5></td>';
       $main .= '<td colspan="3" align="right">';
       $main .= '<a href="/admin/editor" class="btn btn-info" style="font-size: 9px;">Ryd filter</a>';
       $main .= '</td>';
       $main .= '</tr>';


        $main .= '<tr>';

        $main .= '<td width="25%">';
        $main .= '<div class="form-group">';
        $main .= '<label for="projektSelect" style="font-size: 12px;">Projekt/borger</label>';
        $main .= '<select class="form-control-sm filterSelect editorFilter" id="projektSelect">'.$projects.'</select>';
        $main .= '</div>';
        $main .= '</td>';

        $main .= '<td width="25%">';
        $main .= '<div class="form-group">';
        $main .= '<label for="userSelect" style="font-size: 12px;">Medarbejder</label>';
        $main .= '<select class="form-control-sm filterSelect editorFilter" id="userSelect">'.$users.'</select>';
        $main .= '</div>';
        $main .= '</td>';

        $main .= '<td width="25%">';
        $main .= '<div class="form-group">';
        $main .= '<label for="periodeSelect" style="font-size: 12px;">Periode</label>';
        $main .= '<select class="form-control-sm filterSelect editorFilter" id="periodeSelect">'.$perioder.'</select>';
        $main .= '</div>';
        $main .= '</td>';

        $main .= '<td width="25%">';
        $main .= '<div class="form-group">';
        $main .= '<label for="hourTypeSelect" style="font-size: 12px;">Vagt type</label>';
        $main .= '<select class="form-control-sm filterSelect editorFilter" id="hourTypeSelect"></select>';
        $main .= '</div>';
        $main .= '</td>';

        $main .= '</tr>';


        $main .= '<tr>';

        $main .= '<td>';
        $main .= '<div class="form-group">';
        $main .= '<label for="vagtStart" style="font-size: 12px;">Vagt start</label>';
        $main .= '<input class="form-control dateRange editorFilter dateFilter" style="font-size: 10px;" id="vagtStart">';
        $main .= '</div>';
        $main .= '</td>';

        $main .= '<td>';
        $main .= '<div class="form-group">';
        $main .= '<label for="vagtSlut" style="font-size: 12px;">Vagt slut</label>';
        $main .= '<input class="form-control dateRange editorFilter dateFilter" style="font-size: 10px;" id="vagtSlut">';
        $main .= '</div>';
        $main .= '</td>';

        $main .= '<td>';
        $main .= '<div class="form-group">';
        $main .= '<label for="listSorting" style="font-size: 12px;">Godkendelse</label>';
        $main .= '<select class="form-control-sm filterSelect editorFilter" id="approvels">';
        $main .= '<option></option>';
        $main .= '<option value="both">Ingen godkendelse</option>';
        $main .= '<option value="medarbejder">Mangle medarbejder godkendelse</option>';
        $main .= '<option value="borger">Mangle borger godkendelse</option>';
        $main .= '</select>';
        $main .= '</div>';
        $main .= '</td>';

        $main .= '<td>';
        $main .= '<div class="form-group">';
        $main .= '<label for="listSorting" style="font-size: 12px;">Sortering</label>';
        $main .= '<select class="form-control-sm filterSelect editorFilter" id="listSorting">';
        $main .= '<option value="start_date">Start</option>';
        $main .= '<option value="end_date">Slut</option>';
        $main .= '<option value="user_id">Medarbejder</option>';
        $main .= '<option value="project_id">Projekt</option>';
        $main .= '<option value="hour_type_id">Vagt type</option>';
        $main .= '<option value="created_at">Oprettet</option>';
        $main .= '</select>';
        $main .= '</div>';
        $main .= '</td>';

        $main .= '</tr>';

        $main .= '</table>';

        //data list
        $main .= '<h5>Data list</h5>';
        $main .= '<div style="font-size: 10px; cursor: pointer; display: none;" id="selectAll">Vælg alle</div>';

        $main .= '<input id="tableHeader" type="hidden" value="';
        $main .= '<thead>';
        $main .= '<tr>';
        $main .= '<th></th>';
        $main .= '<th>Periode</th>';
        $main .= '<th class=\'sortClick\' style=\'cursor: pointer;\' sortType=\'user_id\'>Medarbejder</th>';
        $main .= '<th class=\'sortClick\' style=\'cursor: pointer;\' sortType=\'project_id\'>Projekt</th>';
        $main .= '<th class=\'sortClick\' style=\'cursor: pointer;\' sortType=\'start_date\'>Start</th>';
        $main .= '<th class=\'sortClick\' style=\'cursor: pointer;\' sortType=\'end_date\'>Slut</th>';
        $main .= '<th class=\'sortClick\' style=\'cursor: pointer;\' sortType=\'hour_type_id\'>Vagt type</th>';
        $main .= '<th class=\'sortClick\' style=\'cursor: pointer;\' sortType=\'created_at\'>Oprettet</th>';
        $main .= '<th>M.G.</th>';
        $main .= '<th>B.G.</th>';
        $main .= '<th>Timer</th>';
        $main .= '</tr>';
        $main .= '</thead>';
        $main .= '">';

        $main .= '<table class="table table-sm" id="hourList" style="font-size: 10px; margin-bottom: 50px;">';
        $main .= '</table>';

        $main .= '<table class="table table-borderless" style="display: none;" id="actionTable">';
        $main .= '<tr>';
        $main .= '<td width="100">Handling</td>';
        $main .= '<td>';
        $main .= '<select id="actionSelect">';
        $main .= '<option></option>';
        $main .= '<option value="a1">Slet</option>';
        $main .= '<option value="a2">Flyt til projekt/borger</option>';
        $main .= '<option value="a3">Ændre medarbejder</option>';
        $main .= '<option value="a4">Fjern medarbejder godkendelse</option>';
        $main .= '<option value="a5">Fjern borger godkendelse</option>';
        $main .= '<option value="a6">Ændre vagt type</option>';
        $main .= '<option value="a7">Set medarbejder godkendelse</option>';
        $main .= '<option value="a8">Set borger godkendelse</option>';
        $main .= '<option value="line">Linje ændringer</option>';
        $main .= '</select>';
        $main .= '</td>';
        $main .= '<td id="actionSetting"></td>';
        $main .= '<td><a class="btn btn-success active" id="actionSetButton" style="display: none;">Set/Udfør</a></td>';
        $main .= '</tr>';
        $main .= '</table>';

        $this->jsFiles[] = 'editor.js';
        $this->displayPage($main);
    }
}