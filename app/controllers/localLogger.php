<?php


namespace app\app\controllers;


use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class localLogger
{
	/**
	 * localLogger constructor.
	 * @throws \Exception
	 */
	public $log;

	public function __construct()
	{
		$this->log = new Logger('name');
		$this->log->pushHandler(new StreamHandler(DIR_ROOT.'/temp/debug.log', Logger::DEBUG));

		return $this;
	}

	public function getLog()
	{
		return $this->log;
	}
}