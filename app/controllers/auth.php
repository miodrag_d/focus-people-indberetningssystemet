<?php


namespace app\app\controllers;
use Firebase\JWT\JWT;
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

class auth
{
	private $app;

	public function __construct(App $app)
	{
		$this->app = $app;
	}

	public function __invoke(Request $request, Response $response, $next)
	{
		$token = $_COOKIE["focusPeople"];

		if(empty($token) or $token == 'undefined')
		{
			return $response->withRedirect('/login');
		}

		if(is_array($token))
		{
			$token = $token[0];
		}

		if(!empty($token))
		{
			$tokenClean = str_replace('Bearer ', '', $token);
			$tokenData = JWT::decode($tokenClean, $_ENV['KEY'], ['HS256']);

			$this->app->getContainer()['userToken'] = $tokenData;
			return $next($request, $response);
		}

		return $response->withRedirect('/login');
	}
}