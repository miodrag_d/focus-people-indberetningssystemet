<?php


namespace app\app\controllers;


use Slim\Http\Request;
use Slim\Http\Response;

class dataGet
{
	private $data;
	private $userProjects = [];

	public function __construct()
	{

		$this->checkForClosePerioeds();
	}

	public function checkCoonection()
	{

	}

	public function loadData()
	{
		$ch = curl_init('https://fpservice.oliviadanmark.dk/api/focuspeople/Authenticate');

		$payload = json_encode( array('username' => 'focuspeople', 'password' => "?5.46pVa_RL*zv#F"));

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		$token = curl_exec($ch);
		curl_close($ch);

		$authorization = "Authorization: Bearer ".substr($token, 1, -1);

		$ch = curl_init('https://fpservice.oliviadanmark.dk/api/focuspeople/get');

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' ,  $authorization));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		$this->data = curl_exec($ch);
		curl_close($ch);
	}

	public function processData()
	{
		if(empty($this->data))
		{
			return false;
		}
		$data = json_decode($this->data, true);

		if(!is_array($data))
		{
			return false;
		}

		foreach ($data['projekter'] as $projektRow)
		{
			$this->handelProject($projektRow);
		}
		$this->handelUserProject();
	}

	public function handelProject($projectData)
	{
	    //dd($projectData);
		//check and update project data
		$project = \ORM::for_table('projects')
			->where('number', $projectData['projektNummer'])
			->where('bevilling_id', $projectData['bevillingID'])
			->find_one();

		if(!$project)
		{
			$project = \ORM::for_table('projects')->create();
            $project->set_expr('created_at', 'NOW()');
            $project->set('active', 1);
		}

		$project->set('number', $projectData['projektNummer']);
		$project->set('name', $projectData['projektnavn']);
		$project->set('bevilling', $projectData['bevilling']);
		$project->set('bevilling_id', $projectData['bevillingID']);
		$project->set_expr('last_update', 'NOW()');
		$project->save();

		//oprettelse og update af borger
		$borger = \ORM::for_table('user')
			->where('user_number', $projectData['projektNummer'])
			->where('user_type', 2)
			->find_one();

		if(!$borger)
		{
			$borger = \ORM::for_table('user')->create();
			$borger->set('created_sms', 1);
		}

		$borger->set('user_number', $projectData['projektNummer']);
		$borger->set('user_type', 2);
		$borger->set('name', $projectData['projektnavn']);
		$borger->set('cell_number', $projectData['projektMobilNummer']);
		$borger->save();

		$this->userProjects[] = ['userId' => $borger->id, 'projektId' => $project->id];

		if($projectData['medarbejder'])
		{
			if(is_array($projectData['medarbejder']))
			{
				foreach ($projectData['medarbejder'] as $medarbejderRow)
				{
					$medarbejder = \ORM::for_table('user')
						->where('user_number', $medarbejderRow['Nummer'])
						->where('user_type', 1)
						->find_one();

					if(!$medarbejder)
					{
						$medarbejder = \ORM::for_table('user')->create();
						$medarbejder->set('created_sms', 1);
					}

					$medarbejder->set('user_number', $medarbejderRow['Nummer']);
					$medarbejder->set('user_type', 1);
					$medarbejder->set('name', $medarbejderRow['Navn']);
					$medarbejder->set('cell_number', str_replace(' ', '', str_replace('NULL', '', $medarbejderRow['mobilnummer'])));
					$medarbejder->save();

					if($medarbejderRow['Navn'] != '<SLETTET> ')
					{
						$this->userProjects[] = ['userId' => $medarbejder->id, 'projektId' => $project->id];
					}
				}
			}
		}

		//time typer
		if($projectData['timetyper'])
		{
			if (is_array($projectData['timetyper'])) {
				$sorting = 1;

				\ORM::raw_execute('UPDATE project_hour_types SET active = null WHERE project_id = ?', [$project->id]);

				foreach (\ORM::for_table('project_hour_types')->find_many() as $hourTypeRow)
				{
					$hourTypeRowTemp[$hourTypeRow['project_id']][$hourTypeRow['number']] = $hourTypeRow;
				}

				foreach ($projectData['timetyper'] as $timeTypeRow)
				{
					$timeType = $hourTypeRowTemp[$project->id][$timeTypeRow['id']];

					if(!$timeType)
					{
						$timeType = \ORM::for_table('project_hour_types')->create();
					}

					$timeType->set('project_id', $project->id);
					$timeType->set('number', $timeTypeRow['id']);
					$timeType->set('name', $timeTypeRow['typenavn']);

					if(substr($timeTypeRow['typenavn'],0,3) == 'ALM')
					{
						$timeType->set('pre_select', 1);
					}

					$timeType->set('sorting', $sorting);
					$timeType->set('active', 1);
					$timeType->save();

					$sorting ++;
				}
			}
		}

	}

	public function handelUserProject()
	{
		//opdating user project
		\ORM::raw_execute('UPDATE user_project SET active = null WHERE internal_user IS null');

		foreach (\ORM::for_table('user_project')->find_many() as $userProjectRow)
		{
			$userProjectTemp[$userProjectRow['user_id']][$userProjectRow['project_id']] = $userProjectRow;
		}

		foreach ($this->userProjects as $userProjectRow)
		{
			$userProject = $userProjectTemp[$userProjectRow['userId']][$userProjectRow['projektId']];


			if(!$userProject)
			{
				$userProject = \ORM::for_table('user_project')->create();
				$userProject->set_expr('created_at', 'NOW()');
			}

			$userProject->set('user_id', $userProjectRow['userId']);
			$userProject->set('project_id', $userProjectRow['projektId']);
			$userProject->set('active', 1);
			$userProject->save();
		}
	}

	public function checkForClosePerioeds()
	{
		$closing = \ORM::for_table('periodes')
			->where_null('locked')
			->where_raw('DATEDIFF(CURDATE(), end_date) > 6')
			->find_one();

		if($closing)
		{
			$closing->set('locked', 1);
			$closing->save();
		}
	}

	public function newUserSMS()
	{
		$users = \ORM::for_table('user')
			->where('created_sms', 1)
			->where_null('created_sms_send')
			->where_raw('LENGTH(cell_number) > 4');


		$sms = new smsSender();
		foreach ($users->find_many() as $user)
		{
			$newPassword = rand(10000,9999999);
			$user->set_expr('password', 'PASSWORD('.$newPassword.')');
			$user->set_expr('created_sms_send', 'NOW()');
			$user->save();

			//afsendelse af SMS
			$sms->sendSMS($user['cell_number'], 'Hej '.$user['name'].'. Dit medarbejder ID/nummer er: '.$user['user_number'].' og din kode er: '.$newPassword.' . Hilsen Focus People');
		}
	}

}