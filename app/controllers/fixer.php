<?php


namespace app\app\controllers;


use Slim\Http\Request;
use Slim\Http\Response;

class fixer extends mainController
{
    public function fixProjects(Request $request, Response $response, $args)
    {
        $oldProjectId = 630;
        $newProjectId = 745;

        $oldProjectData = \ORM::for_table('projects')->where('id', $oldProjectId)->find_one();
        $newProjectData = \ORM::for_table('projects')->where('id', $newProjectId)->find_one();

        dump($oldProjectData->as_array());
        dump($newProjectData->as_array());

        $userProjektCount = \ORM::for_table('user_project')->where('project_id', $newProjectId)->where('active', 1)->count();
        dump($userProjektCount);

        $userWorkerHours = \ORM::for_table('user_work_hours')->where('project_id', $newProjectId)->count();
        dump($userWorkerHours);

        $projectHourTypesCount = \ORM::for_table('project_hour_types')->where('active', 1)->where('project_id', $newProjectId)->count();
        dump($projectHourTypesCount);

        if($projectHourTypesCount > 0)
        {
            \ORM::raw_execute('
            UPDATE
            project_hour_types
            SET
            active = null
            WHERE
            project_id = ?
            ', [$newProjectId]);
        }
    }

    public function sommerTime(Request $request, Response $response, $args)
    {
        $workList = \ORM::for_table('user_work_hours')
            ->where_raw('? BETWEEN start_date AND end_date', [$request->getParam('splitDateTime')])
            ->where('active', 1);

        foreach ($workList->find_many() as $workListRow)
        {
            dump($workListRow);
            $createStart = \ORM::for_table('user_work_hours')->create();
            $createStart->set('user_id', $workListRow['user_id']);
            $createStart->set('project_id', $workListRow['project_id']);
            $createStart->set('periode_id', $workListRow['periode_id']);
            $createStart->set('start_date', $workListRow['start_date']);
            $createStart->set('end_date', $request->getParam('splitDateTime'));
            $createStart->set('hour_type_id', $workListRow['hour_type_id']);
            $createStart->set('created_user_id', $workListRow['created_user_id']);
            $createStart->set_expr('created_at', 'NOW()');
            $createStart->set('user_approved_user_id', $workListRow['user_approved_user_id']);
            $createStart->set('user_approved_at', $workListRow['user_approved_at']);
            $createStart->set('project_approved_user_id', $workListRow['project_approved_user_id']);
            $createStart->set('project_approved_at', $workListRow['project_approved_at']);
            $createStart->set('parent_id', $workListRow['id']);

            $createStart->set('active', 1);
            $createStart->save();

            $createEnd = \ORM::for_table('user_work_hours')->create();
            $createEnd->set('user_id', $workListRow['user_id']);
            $createEnd->set('project_id', $workListRow['project_id']);
            $createEnd->set('periode_id', $workListRow['periode_id']);
            $createEnd->set('start_date', date('Y-m-d H:i:s', strtotime($request->getParam('splitDateTime').' + 1 hour')));
            $createEnd->set('end_date', $workListRow['end_date']);
            $createEnd->set('hour_type_id', $workListRow['hour_type_id']);
            $createEnd->set('created_user_id', $workListRow['created_user_id']);
            $createEnd->set_expr('created_at', 'NOW()');
            $createEnd->set('user_approved_user_id', $workListRow['user_approved_user_id']);
            $createEnd->set('user_approved_at', $workListRow['user_approved_at']);
            $createEnd->set('project_approved_user_id', $workListRow['project_approved_user_id']);
            $createEnd->set('project_approved_at', $workListRow['project_approved_at']);
            $createEnd->set('parent_id', $workListRow['id']);

            $createEnd->set('active', 1);
            $createEnd->save();

            $workListRow->set('active', 2);
            $workListRow->save();

        }
    }

    public function findPeriodeErrors(Request $request, Response $response, $args)
    {
        $fixList = \ORM::for_table('user_work_hours')
            ->join('periodes', 'periodes.start_date <= DATE(user_work_hours.start_date) AND periodes.end_date >= DATE(user_work_hours.end_date) AND periodes.id != user_work_hours.periode_id')
            ->where('user_work_hours.periode_id', '11')
            ->select('user_work_hours.id', 'uid')
            ->select('user_work_hours.periode_id')
            ->select('periodes.*')
            ->find_array();

        dump($fixList);
    }
}