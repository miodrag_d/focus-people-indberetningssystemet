<?php


namespace app\app\controllers;


use Firebase\JWT\JWT;
use Slim\Http\Request;
use Slim\Http\Response;

class apiData extends mainController
{
    public function showHours($hours)
	{
		if($hours > 0)
		{
			return number_format($hours, 2);
		}
	}

	public function removeApproval(Request $request, Response $response, $args)
	{
        $rows = $this->createUserWorkLinesCopy(\ORM::for_table('user_work_hours')->where('active', 1)->where('project_id', $this->projectId)->where('periode_id', $this->periodeId)->where_not_null('project_approved_user_id')->find_array());

        \ORM::raw_execute('
            UPDATE
            user_work_hours
            SET
            project_approved_user_id = null,
            project_approved_at = null
            WHERE
            id IN ('.implode(',', $rows).')
            ');
	}

	public function copyDateList(Request $request, Response $response, $args)
	{
		$getDate = date('Y-m-d', strtotime($args['date']));
		$workHoursFinal = [];
		$workHours = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('user_work_hours.user_id', $this->userId())
			->where('user_work_hours.project_id', $this->projectId)
			->where_raw('DATE(user_work_hours.start_date) = ?', [$getDate])
			->left_outer_join('project_hour_types', 'project_hour_types.id = user_work_hours.hour_type_id AND project_hour_types.project_id = user_work_hours.project_id')
			->select('user_work_hours.*')
			->select('project_hour_types.number')
			->select('project_hour_types.name')
			->order_by_asc('user_work_hours.start_date')
			->find_array();

		foreach ($workHours as $workHoursRow)
		{
			$workHoursFinal[] = [
				'start' => date('H:i', strtotime($workHoursRow['start_date'])),
				'end' => date('H:i', strtotime($workHoursRow['end_date'])),
				'hour_type_id' => $workHoursRow['hour_type_id'],
				'hour_type_name' => $workHoursRow['name']
			];
		}

		return $response->withJson(['hours' => $workHoursFinal]);

 	}

	public function approveWorker(Request $request, Response $response, $args)
	{
		if($this->periodeStatus)
		{
			return false;
		}
		$this->logger->debug('approveWorker', [$args['id']]);
		if($args['id'] == 'all')
		{
			\ORM::raw_execute('
			UPDATE
			user_work_hours
			SET
			project_approved_user_id = ?,
			project_approved_at = NOW()
			WHERE
			project_id = ?
			AND 
			periode_id = ?
			AND
			active = 1
			AND
			user_approved_at IS NOT NULL
			', [
				$this->getSaveUserId(),
				$this->projectId,
				$this->periodeId
			]);

			return $response->withJson(['status' => true]);
		}

		\ORM::raw_execute('
		UPDATE
		user_work_hours
		SET
		project_approved_user_id = ?,
		project_approved_at = NOW()
		WHERE
		user_id = ?
		AND
		project_id = ?
		AND 
		periode_id = ?
		AND 
		active = 1
		AND 
		user_approved_at IS NOT NULL
		', [
			$this->getSaveUserId(),
			$args['id'],
			$this->projectId,
			$this->periodeId
		]);

		return $response->withJson(['status' => true]);
	}

	public function approvHours()
	{
		if($this->periodeStatus)
		{
			return $this->container->response->withJSON(['status' => false]);
		}

		\ORM::raw_execute('
		UPDATE
		user_work_hours
		SET
		user_approved_user_id = ?,
		user_approved_at = NOW()
		WHERE
		user_id = ?
		AND 
		periode_id = ?
		AND 
		project_id = ?
		AND 
		active = 1
		AND 
		project_approved_at IS NULL
		', [
			$this->getSaveUserId(),
			$this->userId(),
			$this->periodeId,
			$this->projectId
		]);

		return $this->container->response->withJSON(['status' => true]);
	}

	public function resetPassword(Request $request, Response $response, $args)
	{
		$postData = $request->getParsedBody();

		if($this->userType() < 3)
		{
			return $response->withJson(['message' => 'Ingen adgang']);
		}

		$user = \ORM::for_table('user')
			->where('id', $postData['userId'])
			->find_one();

		if($user)
		{
			$newPassword = rand(10000,9999999);
			$user->set_expr('password', 'PASSWORD('.$newPassword.')');
			$user->save();

			if($postData['resetType'] == 'show')
			{
				return $response->withJson(['message' => 'Den nye kode er: '.$newPassword]);
			}

			if(strlen($user['cell_number']) == 8)
			{
				$sms = new smsSender();
				$sms->sendSMS($user['cell_number'], 'Hej '.$user['name'].'. Dit medarbejder ID/nummer er: '.$user['user_number'].' og din kode er: '.$newPassword.' . Hilsen Focus People');
				return $response->withJson(['message' => 'Der bliver sendt en ny kode via SMS']);
			}

			return $response->withJson(['message' => 'Mobil nummer mangler']);
		}
	}

	public function getUserToken(Request $request, Response $response, $args)
	{
		$user = \ORM::for_table('user')
			->where('id', $args['id'])
			->find_one();

		$returnToId = '';
		if($user['user_type'] != 3)
		{
			$returnToId = $this->userId();
		}

		if($user)
		{
			$projects = \ORM::for_table('projects')
				->join('user_project', 'user_project.project_id = projects.id')
				->where('user_project.active', 1)
				->select('projects.id')
				->select('projects.number')
				->select('projects.name')
				->where('user_project.user_id', $user['id'])
				->find_array();

			$token = JWT::encode([
				'user' => $user['id'],
				'name' => $user['name'],
				'user_type' => $user['user_type'],
				'projects' => json_encode($projects),
				'returnTo' => $returnToId
			], $_ENV['KEY']);
		}

		return $response->withJson(['token' => $token]);
	}

	public function deleteWorkHour(Request $request, Response $response, $args)
	{
		if($this->periodeStatus)
		{
			return $response->withJson(['status' => false]);
		}

		$getWorkHours = \ORM::for_table('user_work_hours')
			->where('id', $args['id'])
			->where_null('project_approved_at')
			->find_one();

		if($getWorkHours)
		{
			$getWorkHours->set('active', 2);
			$getWorkHours->save();
			return $response->withJson(['status' => true]);
		}

		return $response->withJson(['status' => false]);
	}

	public function lastWorkHourType()
	{
		$hourTypeId = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->select('hour_type_id')
			->where('user_id', $this->userId())
			->where('project_id', $this->projectId)
			->order_by_desc('created_at')
			->find_one();

		return $this->container->response->withJSON(['hour_type_id' => $hourTypeId['hour_type_id']]);
	}

	public function getWorkHours(Request $request, Response $response, $args)
	{
		$getWorkHours = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('id', $args['id'])
			->find_one();

		return $response->withJson([
			'data' => [
				'id' => $getWorkHours['id'],
				'project_approved_user_id' => $getWorkHours['project_approved_user_id'],
				'project_approved_at' => $getWorkHours['project_approved_at'],
				'startDate' => date('Y-m-d', strtotime($getWorkHours['start_date'])),
				'showStartDate' => date('d-m-Y', strtotime($getWorkHours['start_date'])),
				'startTime' => date('H:i:s', strtotime($getWorkHours['start_date'])),
				'endDate' => date('Y-m-d', strtotime($getWorkHours['end_date'])),
				'showEndDate' => date('d-m-Y', strtotime($getWorkHours['end_date'])),
				'endTime' => date('H:i:s', strtotime($getWorkHours['end_date'])),
				'hourTypeId' => $getWorkHours['hour_type_id']
			]
		]);
	}

	public function saveHours(Request $request, Response $response, $args)
	{
        $postData = $request->getParsedBody();
        $this->logger->debug('saveHours', [$postData]);

		if($this->periodeStatus)
		{
			return $response->withJson(['message' => 'Denne periode er lukket for ændringer']);
		}

		if(!empty($postData['copyFromDate']))
		{
			$getDate = date('Y-m-d', strtotime($postData['copyFromDate']));

			\ORM::raw_execute('
			UPDATE
			user_work_hours
			SET
			active = 2
			WHERE
			DATE(start_date) = ?
			AND 
			user_id = ?
			AND 
			project_id = ?
			AND
			active = 1
			', [$postData['startDate'], $this->userId(), $this->projectId]);

			$workHours = \ORM::for_table('user_work_hours')
                ->where('user_work_hours.active', 1)
				->where('user_work_hours.user_id', $this->userId())
				->where('user_work_hours.project_id', $this->projectId)
				->where_raw('DATE(user_work_hours.start_date) = ?', [$getDate])
				->order_by_asc('user_work_hours.start_date')
				->find_array();

			foreach ($workHours as $workHoursRow)
			{
                $periodeFromStartDate = \ORM::for_table('periodes')
                    ->where_raw('start_date <= ?', [date('Y-m-d', strtotime($postData['startDate']))])
                    ->where_raw('end_date >= ?', [date('Y-m-d', strtotime($postData['startDate']))])
                    ->select('id')
                    ->find_one();

				$createRow = \ORM::for_table('user_work_hours')->create();
				$createRow->set('active', 1);
				$createRow->set('user_id', $workHoursRow['user_id']);
				$createRow->set('project_id', $workHoursRow['project_id']);
				$createRow->set('periode_id', $periodeFromStartDate['id']);
				$createRow->set('start_date', $postData['startDate'].' '.date('H:i:s', strtotime($workHoursRow['start_date'])));
				$endDate = $postData['startDate'];
				if(date('Ymd', strtotime($workHoursRow['start_date'])) != date('Ymd', strtotime($workHoursRow['end_date'])))
				{
					$endDate = date('Y-m-d', strtotime($postData['startDate'].' + 1 day'));
				}
				$createRow->set('end_date', $endDate.' '.date('H:i:s', strtotime($workHoursRow['end_date'])));
				$createRow->set('hour_type_id', $workHoursRow['hour_type_id']);
				$createRow->set('created_user_id', $this->getSaveUserId());
				$createRow->set('parent_id', $workHoursRow['id']);
				$createRow->set_expr('created_at', 'NOW()');
				$createRow->save();
			}

			return $response->withJson(['message' => true]);
		}

		$start = $postData['startDate'].' '.$postData['startTime'];
		$end = $postData['endDate'].' '.$postData['endTime'];

        //get periodeId
        $periodeFromStartDate = \ORM::for_table('periodes')
            ->where_raw('start_date <= ?', [date('Y-m-d', strtotime($start))])
            ->where_raw('end_date >= ?', [date('Y-m-d', strtotime($start))])
            ->select('id')
            ->find_one();

        if(!$periodeFromStartDate)
        {
            $periodeFromStartDate = \ORM::for_table('periodes')
                ->where_raw('end_date = ?', [date('Y-m-d', strtotime($end))])
                ->select('id')
                ->find_one();
        }

		if( (strtotime($end) - 1) > (strtotime($this->periodeEnd.' 23:59:59') + 1) )
		{
			return $response->withJson(['message' => 'Dit slut tidspunkt, ligger udenfor perioden.']);
		}

        if( strtotime($start) == strtotime($end) )
        {
            return $response->withJson(['message' => 'FEJL. Din vagt er på 0 minutter. Prøv igen']);
        }

		//check om vagten starter i en anden vagt
		$check = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('user_id', $this->userId())
			->where_raw('
			(
				(? BETWEEN (start_date + INTERVAL 1 SECOND) AND (end_date - INTERVAL 1 SECOND))
				OR
				(? BETWEEN (start_date + INTERVAL 1 SECOND) AND (end_date - INTERVAL 1 SECOND))
				OR
				(? = start_date AND ? = end_date)
			)
			',[
				$start, $end, $start, $end
			]);

		if(!empty($postData['currentId']))
		{
			$check->where_not_equal('id', $postData['currentId']);
		}
		$checkResult = $check->find_one();

		if($checkResult)
		{
		    $this->logger->debug('KONFLIKT', [$start, $end, $checkResult->as_array()]);
			return $response->withJson(['message' => 'Du prøver at oprette en vagt, som er i konflikt, med en allerede oprettet vagt']);
		}

		if(strtotime($end) < strtotime($start))
        {
            return $response->withJson(['message' => 'Slut tidspunktet ligger før start tidspunktet. Prøv igen!']);
        }

		if($postData['hourTypeId'] < 1)
		{
			return $response->withJson(['message' => 'Du skal have valgt en type']);
		}

		if(strtotime($end) - strtotime($start) > (48*60*60))
        {
            return $response->withJson(['message' => 'En samlet vagt må ikke oversige 48 timer']);
        }

        //Check for godkendte timer i perioden.
		if(date('d') == 14 OR date('d') == 15 OR date('d') == 16)
        {
            $godkendteTimer = \ORM::for_table('user_work_hours')
                ->where('active', 1)
                ->where('project_id', $this->projectId)
                ->where('user_id', $this->userId())
                ->where('periode_id', $periodeFromStartDate['id'])
                ->where_not_null('project_approved_user_id')
                ->find_one();

            if($godkendteTimer)
            {
                return $response->withJson(['message' => 'Du har godkendte timer i perioden. Der kan derfor ikke oprettes flere vagter.']);
            }
        }

		if(!empty($postData['currentId']))
		{
            $oldRow = \ORM::for_table('user_work_hours')
                ->where_null('project_approved_at')
                ->where('id', $postData['currentId'])
                ->find_one();

            if($oldRow)
            {
                $oldRow->set('active', 2);
                $oldRow->save();
            }
		}

        $saveHours = \ORM::for_table('user_work_hours')->create();



		if($saveHours AND $this->projectId)
		{
			$saveHours->set('active', 1);
			$saveHours->set('user_id', $this->userId());
			$saveHours->set('project_id', $this->projectId);
			$saveHours->set('periode_id', $periodeFromStartDate['id']);
			$saveHours->set('start_date', $start);
			$saveHours->set('end_date', $end);
			$saveHours->set('hour_type_id', $postData['hourTypeId']);
			$saveHours->set('created_user_id', $this->getSaveUserId());
			$saveHours->set('user_approved_user_id', null);
			$saveHours->set('user_approved_at', null);
			$saveHours->set('parent_id', $oldRow['id']);
			$saveHours->set_expr('created_at', 'NOW()');
			$saveHours->save();
		}

		$this->logger->debug('saveHours', [$postData]);

		return $response->withJson(['message' => true]);
	}

    public function hourCheck(Request $request, Response $response, $args)
    {
        $postData = $request->getParsedBody();

        $start = strtotime($postData['startDate'].' '.$postData['startTime']);
        $end = strtotime($postData['endDate'].' '.$postData['endTime']);

        $total = $end - $start;

        $backCalc = 0;

        if($total > 0)
        {
            $backCalc =   $total/60/60;
        }

        return $response->withJson(['hours' => $backCalc]);
	}

	public function workerInfo(Request $request, Response $response, $args)
	{
		if($this->userType() != 2)
		{
			return false;
		}

		$content = '<table class="table table-bordered" style="font-size: 10px;">';

		$content .= '<thead>';
		$content .= '<tr>';
		$content .= '<th>Start</th>';
		$content .= '<th>Slut</th>';
		$content .= '<th>Timer</th>';
		$content .= '<th>Type</th>';
		$content .= '</tr>';
		$content .= '</thead>';

		$hours = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('user_id', $args['id'])
			->where('user_work_hours.project_id', $this->projectId)
			->where('user_work_hours.periode_id', $this->periodeId)
			->left_outer_join('project_hour_types', 'project_hour_types.id = user_work_hours.hour_type_id')
			->select_expr('SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60)', 'hours')
			->select('user_work_hours.*')
			->select('project_hour_types.name')
			->group_by('user_work_hours.start_date')
			//->group_by_expr('DATE(user_work_hours.start_date)')
			//->group_by('user_work_hours.hour_type_id')
			->order_by_asc('user_work_hours.start_date')
			->find_array();

		foreach ($hours as $hourRow)
		{
			$content .= '<tr>';
			$content .= '<td>'.date('d-m-Y H:i', strtotime($hourRow['start_date'])).'</td>';
			$content .= '<td>'.date('d-m-Y H:i', strtotime($hourRow['end_date'])).'</td>';
			$content .= '<td>'.$this->showHours($hourRow['hours']).'</td>';
			$content .= '<td>'.$hourRow['name'].'</td>';
			$content .= '</tr>';
		}

		$content .= '</table>';

		return $response->withJson(['content' => $content]);
	}

	public function fullList(Request $request, Response $response, $args)
	{
		$list = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('user_work_hours.user_id', $args['id'])
			->left_outer_join('projects', 'projects.id = user_work_hours.project_id')
			->left_outer_join('periodes', 'periodes.id = user_work_hours.periode_id')
			->left_outer_join('project_hour_types', 'project_hour_types.id = user_work_hours.hour_type_id')
			->select_expr('DATE_FORMAT(user_work_hours.start_date, " %d-%m-%Y %H:%i")', 'start_date')
			->select_expr('DATE_FORMAT(user_work_hours.end_date, " %d-%m-%Y %H:%i")', 'end_date')
			->select('user_work_hours.id', 'hourId')
			->select('user_work_hours.hour_type_id')
			->select('projects.number')
			->select('projects.name')
			->select('periodes.locked')
			->select('project_hour_types.name', 'hour_type_name')
			->order_by_desc('user_work_hours.start_date')
			->find_array();

		return $response->withJson(['list' => $list]);
	}

	public function deleteManyWorkHours(Request $request, Response $response, $args)
	{
		$postData = $request->getParsedBody();

		$this->logger->debug('doDelete', [$postData]);

		if(is_array($postData['deleteWorkHours']))
		{
			foreach ($postData['deleteWorkHours'] as $deleteWorkHoursRow)
			{
				$delLine = \ORM::for_table('user_work_hours')->where('id', $deleteWorkHoursRow)->find_one();
				if($delLine)
				{
					$delLine->set('active', 2);
					$delLine->save();
					//$this->logger->debug('doDeleteLine', [$delLine->as_array()]);
				}
			}
		}

		return $response->withJson(['message' => 'Vagter er nu slettet!']);
	}

	public function lookPeriode(Request $request, Response $response, $args)
	{
		$periodes = \ORM::for_table('periodes')
			->where('id', $args['id'])
			->find_one();

		if($periodes)
		{
			$periodes->set('locked', 1);
			$periodes->save();
		}
	}

    public function saveSuperUser(Request $request, Response $response, $args)
    {
        if($this->userType() != 5)
        {
            die;
        }

        $user = \ORM::for_table('user')->where('id', $args['userId'])->find_one();

        if($user)
        {
            if($args['status'] == 'on')
            {
                $user->set('user_type', 4);
            }
            else
            {
                $user->set('user_type', 3);
            }

            $user->save();
        }
	}

    public function getEditorList(Request $request, Response $response, $args)
    {
        $debug = [];
        $data = [];
        $postData = $request->getParsedBody();
        $debug[] = $postData;
        $makeList = false;
        foreach ($postData as $dateRow)
        {
            if(!empty($dateRow))
            {
                $makeList = true;
            }
        }

        if($makeList)
        {
            $list = \ORM::for_table('user_work_hours')
                ->left_outer_join('projects', 'projects.id = user_work_hours.project_id')
                ->left_outer_join('user', 'user.id = user_work_hours.user_id')
                ->left_outer_join('project_hour_types', 'project_hour_types.id = user_work_hours.hour_type_id')
                ->left_outer_join('user', 'ca_user.id = user_work_hours.created_user_id', 'ca_user')
                ->select('user_work_hours.*')

                ->select('user.name', 'user_name')
                ->select('user.user_number', 'user_number')

                ->select('ca_user.name', 'create_user_name')
                ->select('ca_user.user_number', 'create_user_number')

                ->select('project_hour_types.number', 'hour_type_number')
                ->select('project_hour_types.name', 'hour_type_name')

                ->left_outer_join('user', 'ua_user.id = user_work_hours.user_approved_user_id', 'ua_user')
                ->select('ua_user.name', 'user_approv_user_name')
                ->select('ua_user.user_number', 'user_approv_user_number')

                ->left_outer_join('user', 'pa_user.id = user_work_hours.project_approved_user_id', 'pa_user')
                ->select('pa_user.name', 'project_approv_user_name')
                ->select('pa_user.user_number', 'project_approv_user_number')

                ->select('projects.name', 'project_name')
                ->select('projects.number', 'project_number')
                ->select_expr('FORMAT((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60, 2)', 'hoursAmount')
                ->where('user_work_hours.active', 1);

            if(!empty($postData['projekt']))
            {
                $list->where('user_work_hours.project_id', $postData['projekt']);
            }

            if(!empty($postData['user']))
            {
                $list->where('user_work_hours.user_id', $postData['user']);
            }

            if(!empty($postData['periode']))
            {
                $list->where('user_work_hours.periode_id', $postData['periode']);
            }

            if(!empty($postData['hourType']))
            {
                $list->where('user_work_hours.hour_type_id', $postData['hourType']);
            }


            if(!empty($postData['vagtStart']))
            {
                $vagtStart = explode('/', $postData['vagtStart']);
                $startDato[1] = date('Y-m-d', strtotime($vagtStart[0]));
                $startDato[2] = date('Y-m-d', strtotime($vagtStart[1]));

                $list->where_raw('
                (
                  DATE(start_date) BETWEEN ? AND ? 
                )
                ', [$startDato[1], $startDato[2]]);
            }

            if(!empty($postData['vagtEnd']))
            {
                $vagtSlut = explode('/', $postData['vagtEnd']);
                $endDato[1] = date('Y-m-d', strtotime($vagtSlut[0]));
                $endDato[2] = date('Y-m-d', strtotime($vagtSlut[1]));

                $list->where_raw('
                (
                  DATE(end_date) BETWEEN ? AND ? 
                )
                ', [$endDato[1], $endDato[2]]);
            }

            if(!empty($postData['listSorting']))
            {
                $list->order_by_asc($postData['listSorting']);
            }

            if($postData['approvels'] == 'both')
            {
                $list->where_null('user_approved_at');
                $list->where_null('project_approved_at');
            }

            if($postData['approvels'] == 'medarbejder')
            {
                $list->where_null('user_approved_at');
            }

            if($postData['approvels'] == 'borger')
            {
                $list->where_not_null('user_approved_at');
                $list->where_null('project_approved_at');
            }

            $data = $list->find_array();

            /*
            foreach ($list->find_array() as $listRow)
            {
                $data[] = [
                    'fuld' => $listRow,
                    'user_name' => $listRow['user_name'],
                    'user_number' => $listRow['user_number'],
                    '' => $listRow[''],
                    '' => $listRow[''],
                    '' => $listRow[''],
                    '' => $listRow[''],
                    '' => $listRow[''],
                    '' => $listRow[''],
                ];
            }
            */

        }


        return $response->withJson(['debug' => $debug, 'data' => $data]);
	}

    public function removeApprovalWorker(Request $request, Response $response, $args)
    {
        $rows = $this->createUserWorkLinesCopy(\ORM::for_table('user_work_hours')->where('active', 1)->where('user_id', $args['workerId'])->where('periode_id', $this->periodeId)->where_not_null('project_approved_user_id')->find_array());

        \ORM::raw_execute('
            UPDATE
            user_work_hours
            SET
            project_approved_user_id = null,
            project_approved_at = null
            WHERE
            id IN ('.implode(',', $rows).')
            ');
	}

}