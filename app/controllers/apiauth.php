<?php


namespace app\app\controllers;
use Firebase\JWT\JWT;
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

class apiauth
{
	private $app;

	public function __construct(App $app)
	{
		$this->app = $app;
	}

	public function __invoke(Request $request, Response $response, $next)
	{
		$token = $request->getHeader("Authorization");

		if(empty($token))
		{
			return $response->withStatus(400);
		}

		if(is_array($token))
		{
			$token = $token[0];
		}

		if(!empty($token))
		{
			$tokenClean = str_replace('Bearer ', '', $token);
			$tokenData = JWT::decode($tokenClean, $_ENV['KEY'], ['HS256']);

			if($request->getServerParam('REMOTE_ADDR') == $tokenData->ip AND $_ENV['DATA_USER'] == $tokenData->user AND $_ENV['DATA_PASSWORD'] == $tokenData->password)
			{
				return $next($request, $response);
			}
		}

		return $response->withStatus(400);
	}


}