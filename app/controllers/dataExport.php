<?php


namespace app\app\controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class dataExport
{
	public function getStatus(Request $request, Response $response, $args)
	{
		$statusLines = [];
		$status = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->left_outer_join('user', 'user.id = user_work_hours.user_id')
			->left_outer_join('projects', 'projects.id = user_work_hours.project_id')
			->left_outer_join('periodes', 'periodes.id = user_work_hours.periode_id')
			->select_expr('SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60)', 'totalTimer')
			->select_expr('(CASE WHEN user_approved_at IS NOT NULL THEN SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60) else 0 END)', 'totalTimerAp1')
			->select_expr('(CASE WHEN project_approved_at IS NOT NULL THEN SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60) else 0 END)', 'totalTimerAp2')
			->select('user.name', 'user_name')
			->select('user.user_number', 'user_number')
			->select('periodes.*')
			->select('projects.name', 'projects_name')
			->select('projects.number', 'projects_number')
			->select('projects.bevilling_id', 'projects_bevilling_id')
			->where_null('user.internal_user')
			->group_by('periodes.id')
			->group_by('projects.id')
			->group_by('user.id');

		if(!empty($request->getParam('startDate')))
		{
			$status->where_raw('DATE(user_work_hours.start_date) >= ?', [date('Y-m-d', strtotime($request->getParam('startDate')))]);
		}

		if(!empty($request->getParam('endDate')))
		{
			$status->where_raw('DATE(user_work_hours.end_date) <= ?', [date('Y-m-d', strtotime($request->getParam('endDate')))]);
		}

        if(empty($request->getParam('showLocked')))
        {
            $status->where_null('periodes.locked');
        }

		foreach ($status->find_array() as $statusRow)
		{
			$linestatus = 'Mangler medarbejder godkendelse';
			if($statusRow['totalTimer'] == $statusRow['totalTimerAp1'])
			{
				$linestatus = 'Medarbejder godkendt';
			}

			if($statusRow['totalTimer'] == $statusRow['totalTimerAp2'])
			{
				$linestatus = 'Projekt godkendt';
			}

			$medarbejderGodkendelseProcent = 0;
			if($statusRow['totalTimerAp1'] > 0)
            {
                $medarbejderGodkendelseProcent = ($statusRow['totalTimer'] / $statusRow['totalTimerAp1']) * 100;
            }

            $projectGodkendelseProcent = 0;
            if($statusRow['totalTimerAp1'] > 0)
            {
                $projectGodkendelseProcent = ($statusRow['totalTimer'] / $statusRow['totalTimerAp2']) * 100;
            }

			$statusLines[date('dmY', strtotime($statusRow['start_date'])).'_'.date('dmY', strtotime($statusRow['end_date']))][] = [
				'totalTimer' => number_format($statusRow['totalTimer'], 2),
				'periode' => date('d-m-Y', strtotime($statusRow['start_date'])).' '.date('d-m-Y', strtotime($statusRow['end_date'])),
				'medarbejderGodkendtTimer' => number_format($statusRow['totalTimerAp1'], 2),
				'medarbejderGodkendtProcent' => number_format($medarbejderGodkendelseProcent, 0),
				'projektGodkendtTimer' => number_format($statusRow['totalTimerAp2'], 2),
				'projektGodkendtProcent' => number_format($projectGodkendelseProcent, 0),
				'medarbejderNavn' => $statusRow['user_name'],
				'medarbejderNummer' => $statusRow['user_number'],
				'projektNavn' => $statusRow['projects_name'],
				'projektNummer' => $statusRow['projects_number'],
				'status' => $linestatus
			];
		}

		return $response->withJson(['perioder' => $statusLines]);
	}

	public function dataExport(Request $request, Response $response, $args)
	{
		$list = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->join('user', 'user.id = user_work_hours.user_id')
			->left_outer_join('periodes', 'periodes.id = user_work_hours.periode_id')
			->left_outer_join('projects', 'projects.id = user_work_hours.project_id')
			->where_not_null('projects.name')
			->where_not_null('user_work_hours.user_approved_user_id')
			->where_not_null('user_work_hours.project_approved_user_id')

            ->left_outer_join('project_hour_types', 'project_hour_types.id = user_work_hours.hour_type_id')

			->select_expr('CONCAT(periodes.start_date," ",periodes.end_date)', 'period')
			->select_expr('DATE_FORMAT(user_work_hours.start_date, "%H:%i")', 'start')
			->select_expr('DATE_FORMAT(user_work_hours.end_date, "%H:%i")', 'end')

			//->select('user_work_hours.end_date')
			->select_expr('DATE_FORMAT(user_work_hours.start_date, "%Y-%m-%d")', 'date')
			->select_expr('DATE_FORMAT(user_work_hours.end_date, "%Y-%m-%d")', 'endDate')

			->select('projects.name', 'project_name')
			->select('projects.number', 'project_number')
			->select('projects.bevilling', 'project_bevilling')
			->select('projects.bevilling_id', 'project_bevillingId')
			->select('user.name', 'user_name')
			->where_null('user.internal_user')
			->select('user.user_number', 'user_number')
			->select('project_hour_types.number', 'hour_type_number')
			->select('project_hour_types.name', 'hour_type_name')
			->select_expr('(unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60', 'hoursAmount')
            ->where_raw('(unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60 > 0');

		if(!empty($request->getParam('startDate')))
		{
			$list->where_raw('DATE(user_work_hours.start_date) >= ?', [date('Y-m-d', strtotime($request->getParam('startDate')))]);
		}

		if(!empty($request->getParam('endDate')))
		{
			$list->where_raw('DATE(user_work_hours.start_date) <= ?', [date('Y-m-d', strtotime($request->getParam('endDate')))]);
		}

		if(!empty($request->getParam('projectNumber')))
		{
			$list->where('projects.number', $request->getParam('projectNumber'));
		}

        if(!empty($request->getParam('bevillingID')))
        {
            $list->where('projects.bevilling_id', $request->getParam('bevillingID'));
        }

        if(!empty($request->getParam('userNumber')))
        {
            $list->where('user.user_number', $request->getParam('userNumber'));
        }

        if(!empty($request->getParam('periodeId')))
        {
            $list->where('user_work_hours.periode_id', $request->getParam('periodeId'));
        }

		//dump($list->find_array());

        //$totalTimer = 0;

        if(!empty($request->getParam('split')))
        {
            $masterOutput = [];

            foreach ($list->find_array() as $dataRow)
            {
                //$totalTimer = $totalTimer + $dataRow['hoursAmount'];
                //dump($dataRow);

                //skud dag check
                $startNextDay = '2020-01-01 '.$dataRow['start'];
                $endNextDay = date('Y-m-d', strtotime('+ '.(int)$dataRow['hoursAmount'].' hours', strtotime('2020-01-01 '.$dataRow['start']))).' '.$dataRow['end'];
                $nextDayHours = (strtotime($endNextDay) - strtotime($startNextDay))/60/60;

                // skrift til vintertid
                if($dataRow['hoursAmount'] != $nextDayHours AND $nextDayHours > 0 AND $dataRow['hoursAmount'] > 24)
                {
                    $dataEdit = $dataRow;
                    $dataEdit['hoursAmount'] = "24";
                    $dataEdit['end'] = $dataEdit['start'];
                    $masterOutput[] = $dataEdit;

                    $dataEdit['date'] = $dataEdit['endDate'];
                    $dataEdit['end'] = date('H:i', strtotime('+ '.(($dataRow['hoursAmount'] - 24) * 60).' minutes', strtotime($dataRow['endDate']. ' '.$dataRow['start'])));
                    //$dataEdit['end'] = date('H:i', strtotime('+ '.($dataRow['hoursAmount'] - 24).' hours', strtotime($dataRow['endDate']. ' '.$dataRow['end'])));
                    $dataEdit['hoursAmount'] = $dataRow['hoursAmount'] - 24;

                    $masterOutput[] = $dataEdit;
                }
                elseif($dataRow['hoursAmount'] != $nextDayHours AND $nextDayHours > 0 AND $dataRow['hoursAmount'] < 24)
                {
                    $dataEdit = $dataRow;

                    $dataEdit['end'] = date('H:i', strtotime('+ 1 hours', strtotime($dataRow['endDate']. ' '.$dataRow['end'])));
                    $masterOutput[] = $dataEdit;
                }
                elseif($dataRow['hoursAmount'] > 24 AND $dataRow['start'] != $dataRow['end'])
                {
                    //First 24
                    $dataEdit = $dataRow;
                    $dataEdit['hoursAmount'] = "24";
                    $dataEdit['end'] = $dataEdit['start'];
                    $masterOutput[] = $dataEdit;

                    $dataEdit = $dataRow;
                    $dataEdit['hoursAmount'] = (string)($dataRow['hoursAmount'] - 24);
                    $dataEdit['date'] = $dataRow['endDate'];
                    $masterOutput[] = $dataEdit;
                }
                else
                {
                    $masterOutput[] = $dataRow;
                }

            }
            //dd($totalTimer);
            return $response->withJson($masterOutput);
        }

        return $response->withJson($list->find_array());
	}

    public function workManyHours(Request $request, Response $response, $args)
    {
        $list = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
            ->left_outer_join('user', 'user.id = user_work_hours.user_id')
            ->left_outer_join('periodes', 'periodes.id = user_work_hours.periode_id')
            ->left_outer_join('projects', 'projects.id = user_work_hours.project_id')

            ->select_expr('CONCAT(periodes.start_date," ",periodes.end_date)', 'period')
            ->select_expr('DATE_FORMAT(user_work_hours.start_date, "%H:%i")', 'start')
            ->select_expr('DATE_FORMAT(user_work_hours.end_date, "%H:%i")', 'end')
            ->select('user.name', 'user_name')
            ->select('user.user_number', 'user_number')
            ->where_null('user.internal_user')

            ->select('projects.name', 'project_name')
            ->select('projects.number', 'project_number')
            ->select('projects.bevilling', 'project_bevilling')
            ->select('projects.bevilling_id', 'project_bevillingId')

            ->select_expr('(unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60', 'hoursAmount')
            ->where_raw('((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60) > 24');

        if(!empty($request->getParam('userNumber')))
        {
            $list->where('user.user_number', $request->getParam('userNumber'));
        }

        if(!empty($request->getParam('startDate')))
        {
            $list->where_raw('DATE(user_work_hours.start_date) >= ?', [date('Y-m-d', strtotime($request->getParam('startDate')))]);
        }

        if(!empty($request->getParam('endDate')))
        {
            $list->where_raw('DATE(user_work_hours.end_date) <= ?', [date('Y-m-d', strtotime($request->getParam('endDate')))]);
        }


        return $response->withJson($list->find_array());
	}
}