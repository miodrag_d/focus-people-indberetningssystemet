<?php


namespace app\app\controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class editorApi extends mainController
{

    public function getHourTypes(Request $request, Response $response, $args)
    {
        if(empty($_GET['projectType']))
        {
            $workRow = \ORM::for_table('user_work_hours')->where('id', $args['rowId'])->find_one();
        }
        else
        {
            $workRow['project_id'] = $args['rowId'];
        }

        $hourTypes = \ORM::for_table('project_hour_types')
            ->where('active', 1)
            ->order_by_asc('sorting')
            ->where('project_id', $workRow['project_id'])
            ->find_array();

        return $response->withJson(['data' => $hourTypes]);
    }

    public function actionHandler(Request $request, Response $response, $args)
    {
        $postData = $request->getParsedBody();

        if(is_array($postData))
        {
            foreach ($postData as $postDataRow)
            {
                $workLine = \ORM::for_table('user_work_hours')
                    ->where('id', $postDataRow['rowId'])
                    ->find_one();

                if($workLine)
                {
                    $workLine->set('active', 2);
                    $workLine->save();

                    $workLineArray = $workLine->as_array();
                    $workLineArray['id'] = '';

                    $newWorkLine = \ORM::for_table('user_work_hours')->create($workLineArray);
                    $newWorkLine->set('created_user_id', $this->getSaveUserId());
                    $newWorkLine->set_expr('created_at', 'NOW()');
                    $newWorkLine->set('parent_id', $workLine['id']);

                    if(!empty($postDataRow['newHourType']))
                    {
                        $this->logger->debug('actionHandlerLineType', ['newHourType']);
                        $newWorkLine->set('hour_type_id', $postDataRow['newHourType']);
                    }

                    if(!empty($postDataRow['newStartDate']) AND $postDataRow['actionType'] == 'line')
                    {
                        $this->logger->debug('actionHandlerLineType', ['newStartDate']);
                        $newWorkLine->set('start_date', date('Y-m-d H:i:s', strtotime($postDataRow['newStartDate'])));
                    }

                    if(!empty($postDataRow['newEndDate']) AND $postDataRow['actionType'] == 'line')
                    {
                        $this->logger->debug('actionHandlerLineType', ['newEndDate']);
                        $newWorkLine->set('end_date', date('Y-m-d H:i:s', strtotime($postDataRow['newEndDate'])));
                    }

                    if(!empty($postDataRow['deleteRow']))
                    {
                        $this->logger->debug('actionHandlerLineType', ['deleterow']);
                        $newWorkLine->set('active', 2);
                    }
                    else
                    {
                        $newWorkLine->set('active', 1);
                    }

                    if(!empty($postDataRow['newProjectId']))
                    {
                        $this->logger->debug('actionHandlerLineType', ['newProjectId']);
                        $newWorkLine->set('project_id', $postDataRow['newProjectId']);
                    }

                    if(!empty($postDataRow['newUserId']))
                    {
                        $this->logger->debug('actionHandlerLineType', ['newUserId']);
                        $newWorkLine->set('user_id', $postDataRow['newUserId']);
                    }

                    if(!empty($postDataRow['removeUserApprovel']))
                    {
                        $this->logger->debug('actionHandlerLineType', ['removeUserApprovel']);
                        $newWorkLine->set('user_approved_user_id', null);
                        $newWorkLine->set('user_approved_at', null);
                    }

                    if(!empty($postDataRow['removeProjectApprovel']))
                    {
                        $this->logger->debug('actionHandlerLineType', ['removeProjectApprovel']);
                        $newWorkLine->set('project_approved_user_id', null);
                        $newWorkLine->set('project_approved_at', null);
                    }

                    if(!empty($postDataRow['setUserApprovel']))
                    {
                        $this->logger->debug('actionHandlerLineType', ['setUserApprovel']);
                        $newWorkLine->set('user_approved_user_id', $this->getSaveUserId());
                        $newWorkLine->set_expr('user_approved_at', 'NOW()');
                    }

                    if(!empty($postDataRow['setProjectApprovel']))
                    {
                        $this->logger->debug('actionHandlerLineType', ['setProjectApprovel']);


                        if(is_null($newWorkLine->get('user_approved_user_id')))
                        {
                            $newWorkLine->set('user_approved_user_id', $this->getSaveUserId());
                            $newWorkLine->set_expr('user_approved_at', 'NOW()');
                        }
                        
                        $newWorkLine->set('project_approved_user_id', $this->getSaveUserId());
                        $newWorkLine->set_expr('project_approved_at', 'NOW()');
                    }

                    $newWorkLine->save();

                    $this->logger->debug('actionHandlerLine', [implode('-', $postDataRow), array_keys($postDataRow)]);

                }
            }
        }


        $this->logger->debug('actionHandler', [array_keys($postData[0])]);

        return $response->withJson(['status' => true]);
    }

    public function checkHourEdit(Request $request, Response $response, $args)
    {
        $postData = $request->getParsedBody();

        $start = date('Y-m-d H:i:s', strtotime($postData['startDate']));
        $end = date('Y-m-d H:i:s', strtotime($postData['startEnd']));

        $rowData = \ORM::for_table('user_work_hours')->where('id', $postData['rowId'])->find_one();

        $this->logger->debug('konflikt check', [$start, $end, $postData['rowId'], implode(',',$rowData), array_keys($rowData->as_array())]);

        //check om vagten starter i en anden vagt
        $check = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
            ->where('user_id', $rowData['user_id'])
            ->where_raw('
			(
				(? BETWEEN (start_date + INTERVAL 1 SECOND) AND (end_date - INTERVAL 1 SECOND))
				OR
				(? BETWEEN (start_date + INTERVAL 1 SECOND) AND (end_date - INTERVAL 1 SECOND))
				OR
				(? = start_date AND ? = end_date)
			)
			',[
                $start, $end, $start, $end
            ]);

        $check->where_not_equal('id', $rowData['id']);
        $checkResult = $check->find_one();

        if($checkResult)
        {
            $this->logger->debug('KONFLIKT', [$start, $end, $checkResult->as_array()]);
            return $response->withJson(['message' => 'Konflikt med en allerede oprettet vagt']);
        }

        return $response->withJson(['message' => '']);
    }

    public function workHourRowLog(Request $request, Response $response, $args)
    {
        $parentId = $args['rowId'];
        $returnList = [];
        while (!empty($parentId))
        {
            $rowData = \ORM::for_table('user_work_hours')
                ->where('user_work_hours.id', $parentId)
                ->select('user_work_hours.*')

                ->left_outer_join('user', 'user.id = user_work_hours.user_id')
                ->select('user.user_number', 'user_number')
                ->select('user.name', 'user_name')

                ->left_outer_join('user', 'ca_user.id = user_work_hours.user_id', 'ca_user')
                ->select('ca_user.user_number', 'ca_user_number')
                ->select('ca_user.name', 'ca_user_name')

                ->left_outer_join('projects', 'projects.id = user_work_hours.project_id')
                ->select('projects.name', 'project_name')
                ->select('projects.number', 'project_number')

                ->left_outer_join('project_hour_types', 'project_hour_types.id = user_work_hours.hour_type_id')
                ->select('project_hour_types.number', 'hour_type_number')
                ->select('project_hour_types.name', 'hour_type_name')

                ->find_one();
            $parentId = $rowData['parent_id'];

            $returnList[] = $rowData->as_array();
        }

        $this->logger->debug('logList', [$args]);
        return $response->withJson(['logList' => $returnList]);
    }

}