<?php


namespace app\app\controllers;
use League\Plates\Engine;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Slim\Container;

class mainController
{
	public $container;
	public $logger;
	public $periodeId;
	public $periodeStart;
	public $periodeEnd;
	public $periodeStatus;
	public $jsFiles;
	public $projects;
	public $projectId;
	public $projectNumber;
	public $projectName;
	public $projectCount;
	public $mobile;

	public function __construct(Container $container)
	{
		//dump($app);
		$this->container = $container;

		$log = new localLogger();
		$this->logger = $log->getLog();

		$this->handelProjects();
		$this->readFocusData();
	}

	public function handelProjects()
	{
		if($this->container->has('userToken'))
		{
			$projects = json_decode($this->container['userToken']->projects, true);

			if(empty($_SESSION['projektSelect']))
			{
				$_SESSION['projektSelect'] = 0;
			}

			$this->projectId = $projects[$_SESSION['projektSelect']]['id'];
			$this->projectNumber = $projects[$_SESSION['projektSelect']]['number'];
			$this->projectName = $projects[$_SESSION['projektSelect']]['name'];
			$this->projectCount = count($projects);
			$this->projects = $projects;

		}
	}

	public function userId()
	{
		return $this->container['userToken']->user;
	}

	public function userName()
	{
		return $this->container['userToken']->name;
	}

	public function userType()
	{
		return $this->container['userToken']->user_type;
	}

	public function returnUser()
	{
		return $this->container['userToken']->returnTo;
	}

	public function setFocusC($periode)
	{
		$_SESSION['periode'] = json_encode($periode);
	}

    public function getSaveUserId()
    {
        if(!empty($this->container['userToken']->returnTo))
        {
            return $this->container['userToken']->returnTo;
        }

        return $this->userId();
	}

	public function readFocusData()
	{
		if(!empty($_SESSION['periode']))
		{
			$dataSplit['periode'] = json_decode($_SESSION['periode'], true);
		}
		else
		{
			$periode = \ORM::for_table('periodes')
				->where_raw('
				start_date <= CURDATE()
				AND
				end_date >= CURDATE()
				')
				->find_one();

			if(!$periode)
			{
				$periode = \ORM::for_table('periodes')->create();
				$periode->set('start_date', date('Y-m-14', strtotime('this month')));
				$periode->set('end_date', date('Y-m-13', strtotime('next month')));
				$periode->save();
			}


			$dataSplit['periode'] = $periode->as_array();
			$this->logger->debug('setting period', [$dataSplit]);
			$this->setFocusC($dataSplit['periode']);
		}

		$this->periodeId = $dataSplit['periode']['id'];
		$this->periodeStart = $dataSplit['periode']['start_date'];
		$this->periodeEnd = $dataSplit['periode']['end_date'];
		$this->periodeStatus = $dataSplit['periode']['locked'];
	}

    public function createUserWorkLinesCopy($rows = array())
    {
        if(count($rows) > 0)
        {
            $newRows = [];
            $newRowIds = [];
            $rowIds = [];
            $keys = array_keys($rows[0]);
            unset($keys[0]);

            foreach ($rows as $rowLine)
            {
                $oldId = $rowLine['id'];
                $rowIds[] = $rowLine['id'];

                unset($rowLine['id']);

                foreach ($keys as $keysRow)
                {
                    if($keysRow == 'id')
                    {
                        continue;
                    }

                    if($keysRow == 'created_user_id')
                    {
                        $rowLine[$keysRow] = $this->userId();
                    }

                    if($keysRow == 'created_at')
                    {
                        $rowLine[$keysRow] = date('Y-m-d H:i:s');
                    }

                    if($keysRow == 'parent_id')
                    {
                        $rowLine[$keysRow] = $oldId;
                    }

                }

                $create = \ORM::for_table('user_work_hours')->create($rowLine);
                $create->save();

                $newRowIds[] = $create->id;
            }

            \ORM::raw_execute('
            UPDATE
            user_work_hours
            SET
            active = 2
            WHERE
            id IN ('.implode(',', $rowIds).')
            ');

            return $newRowIds;
        }
	}

	public function displayPage($content)
	{
		$returnButton = '';
		if($this->container['userToken']->returnTo)
		{
			$this->jsFiles[] = 'mainAdminPage.js';
			$returnButton = ' <button class="btn btn-xs btn-danger logInAs" userId="'.$this->container['userToken']->returnTo.'">Retur</button>';
		}

		$jefiles = '';
		foreach ($this->jsFiles as $jsRow)
		{
			$jefiles .= '<script src="/js/'.$jsRow.'?v='.time().'"></script>';
		}

		$statusIcon = '';
		if($this->periodeStatus)
		{
			$statusIcon = '<i class="fa fa-lock" style="color: red;"></i>';
		}

		$projectIcon = '';
		if($_ENV['SITE_SERVICE'] == 1)
		{
			$projectIcon = '<i style="color: red;"> SERVICE</i>';
			if($_SERVER['REMOTE_ADDR'] != '128.76.128.194')
			{
				$content = '<H1 style="color: red;">SIDEN ER UNDER OPDATERING. <br>KLAR IGEN INDEN FOR EN TIME</H1>';
			}
		}

		if($this->projectCount > 1)
		{
			$projectIcon .= ' <i class="fa fa-bars"></i>';
		}

		$topMenu = '';

        $topMenu .= '<li class="nav-item">
					<a class="nav-link" href="/">Home</a>
				</li>';

        if($this->userType() == 5)
        {
            $topMenu .= '<li class="nav-item">
					<a class="nav-link" href="/admin/users">Super users</a>
				</li>';
        }

        if($this->userType() >= 4)
        {
            $topMenu .= '<li class="nav-item">
					<a class="nav-link" href="/admin/editor">Vagt redigering</a>
				</li>';
        }

		if($this->userType() >= 3)
        {
            $topMenu .= '<li class="nav-item">
					<a class="nav-link" href="/perioder">Perioder</a>
				</li>';

            $topMenu .= '<li class="nav-item">
					<a class="nav-link" href="/admin/rapportlist">Rapporter</a>
				</li>';
        }

		$template = new Engine(DIR_SRC.'templates');
		$template->addData([
		    'topMenu' => $topMenu,
			'jsFiles' => $jefiles,
			'content' => $content,
			'userName' => $this->userName(),
			'projektName' => $this->projectName.$projectIcon,
			'periode' => $statusIcon.date('d-m-Y', strtotime($this->periodeStart)).' - '.date('d-m-Y', strtotime($this->periodeEnd)),
			'returnBotton' => $returnButton
		]);
		echo $template->render('site.html');
	}



}