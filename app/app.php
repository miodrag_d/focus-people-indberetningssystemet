<?php

ini_set('display_errors',false);
error_reporting(E_ALL);
session_start();
const ORM_CONNECTION_WRITER = ORM::DEFAULT_CONNECTION;

//database
ORM::configure([
	'connection_string' => 'mysql:host=' .$_ENV['DATABASE_HOSTNAME'] . ';dbname=' . $_ENV['DATABASE_SCHEMA'],
	'username' => $_ENV['DATABASE_USERNAME'],
	'password' => $_ENV['DATABASE_PASSWORD'],

],
	null);

ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
