<?php


namespace app\app\pages;
use app\app\controllers\mainController;
use app\app\controllers\smsSender;
use Firebase\JWT\JWT;
use Slim\Http\Request;
use Slim\Http\Response;

class mainPage extends mainController
{
	public function index(Request $request, Response $response, $args)
	{
		if($this->userType() == 1)
		{
			$content = self::workerPage();
		}

		if($this->userType() == 2)
		{
			$content = self::projectPage();
		}

		if($this->userType() >= 3)
		{
			$content = self::adminPage();
		}

		$main = '';

		$main .= '
		<div class="row">
			<div class="col-lg-12">'.$content.'</div>
		</div>
		';

		$this->displayPage($main);
	}

	public function weekDayLetter($letter)
	{
		if($letter == 'Wed') return 'O';
		if($letter == 'Thu') return 'T';
		if($letter == 'Fri') return 'F';
		if($letter == 'Sat') return 'L';
		if($letter == 'Sun') return 'S';
		if($letter == 'Mon') return 'M';
		if($letter == 'Tue') return 'T';

	}

	public function workerPage()
	{
		$main = '';

		//loading user data
		$userWorkData = [];
		$lastEndHour = [];
		$lastEndDate = '';

		$userData = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('user_work_hours.user_id', $this->userId())
			->where('user_work_hours.periode_id', $this->periodeId)
			->where('user_work_hours.project_id', $this->projectId)
			->left_outer_join('project_hour_types', 'project_hour_types.id = user_work_hours.hour_type_id AND project_hour_types.project_id = user_work_hours.project_id')
			->select('user_work_hours.*')
			->select('project_hour_types.number')
			->select('project_hour_types.name')
			->order_by_asc('start_date');

		foreach ($userData->find_array() as $userDataRow)
		{
			$barColor = 'background: rgb(88,35,39); background: linear-gradient(90deg, rgba(88,35,39,1) 0%, rgba(195,65,74,1) 100%);';

			if($userDataRow['user_approved_user_id'])
			{
				$barColor = 'background: rgb(36,33,0); background: linear-gradient(90deg, rgba(36,33,0,1) 0%, rgba(207,205,48,1) 100%);';
			}

			if($userDataRow['project_approved_user_id'])
			{
				$barColor = 'background: rgb(0,36,14); background: linear-gradient(90deg, rgba(0,36,14,1) 0%, rgba(46,202,172,1) 100%);';
			}

			$lineText = date('H:i', strtotime($userDataRow['start_date']));
			$titel = date('H:i', strtotime($userDataRow['start_date'])).' - '.date('H:i', strtotime($userDataRow['end_date']))."\n Type: ".$userDataRow['name'];

			$startHour = date('G', strtotime($userDataRow['start_date']));
			if(date('G', strtotime($userDataRow['start_date'])) == $lastEndHour AND $lastEndDate == date('Ymd', strtotime($userDataRow['start_date'])))
			{
				$startHour ++;
			}

			if(date('Y-m-d', strtotime($userDataRow['start_date'])) != date('Y-m-d', strtotime($userDataRow['end_date'])))
			{
				$userWorkData[date('Ymd', strtotime($userDataRow['start_date']))][] = '
				<li style="grid-column: '.($startHour + 1).'/25; '.$barColor.' height: 23px;" class="workHours" editId="'.$userDataRow['id']. '" title="'.$titel.'">'.$lineText.'</li>
				';

				if(date('H:i:s', strtotime($userDataRow['end_date'])) != '00:00:00')
				{
					$userWorkData[date('Ymd', strtotime($userDataRow['end_date']))][] = '
					<li style="grid-column: 1/'.(date('G', strtotime($userDataRow['end_date'])) + 1).'; '.$barColor.' height: 23px;" class="workHours" editId="'.$userDataRow['id'].'" title="'.$titel.'">'.$lineText.'</li>
					';
				}
			}
			else
			{
				$userWorkData[date('Ymd', strtotime($userDataRow['start_date']))][] = '
				<li style="grid-column: '.($startHour + 1).'/'.(date('G', strtotime($userDataRow['end_date'])) + 1).'; '.$barColor.' height: 23px;" class="workHours" editId="'.$userDataRow['id'].'" title="'.$titel.'">'.$lineText.'</li>
			';
			}

			$lastEndHour = date('G', strtotime($userDataRow['end_date']) - 1);
			$lastEndDate = date('Ymd', strtotime($userDataRow['end_date']));
		}

		$displayHours = '';
		for($step = 0; $step < 24; $step ++)
		{
			$displayHours .= '<span>'.$step.'</span>';
		}

		$dates = [];
		$runDate = $this->periodeStart;

		for($step = 1; $step < 35; $step ++)
		{
			$dates[] = $runDate;

			if($runDate == $this->periodeEnd)
			{
				break;
			}

			$runDate = date('Y-m-d', strtotime('+ 1 day', strtotime($runDate)));
		}

		$displaDates = '';
		foreach ($dates as $dateRow)
		{
			$displaDates .= '<div class="gantt__row" style="height: 25px;">
			<div class="gantt__row-first dateRow" style="z-index: 3; cursor: pointer; border-right: #4e555b solid 1px;" rowDate="'.date('Y-m-d', strtotime($dateRow)).'" displayDate="'.date('d-m-Y', strtotime($dateRow)).'">
				<div class="row">
					<div class="col-1">
						'.$this->weekDayLetter(date('D', strtotime($dateRow))).'
					</div>
					<div class="col-7">
						'.date('d-m-Y', strtotime($dateRow)).'	
					</div>
					<div class="col-1">
						<i class="fa fa-plus-circle"></i>	
					</div>
				</div>
				
			</div>
			<ul class="gantt__row-bars" >
			'.implode('', $userWorkData[date('Ymd', strtotime($dateRow))]).'
			</ul>
		</div>';
		}

		$main = '
			<div class="gantt">
				<div class="gantt__row gantt__row--months">
					<div class="gantt__row-first"></div>
					'.$displayHours.'
				</div>
		<div class="gantt__row gantt__row--lines">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
		'.$displaDates.'
		</div>
		<div class="row" style="height: 30px;"></div>
		';


		//totals
		$main .= '<div class="row" style="background-color: #0a3444; color: #FFFFFF; font-weight: bold; padding: 10px;">Totaler</div>';
		$main .= '<div class="row" style="background-color: #FFFFFF; padding: 10px;">
					<div class="col-6">
						'.$this->getTotals().'
					</div>
					<div class="col-6" style="text-align: center;">
						'.$this->getApprovel().'
					</div>
					</div>';


		$main .= '<div class="row" style="height: 30px;"></div>';

		$vagtHours = '';
		for ($step = 0; $step < 24; $step ++)
		{
			$vagtHours .= '<option value="'.str_pad($step, 2, '0', STR_PAD_LEFT).':00:00">'.str_pad($step, 2, '0', STR_PAD_LEFT).':00</option>';
			$vagtHours .= '<option value="'.str_pad($step, 2, '0', STR_PAD_LEFT).':15:00">'.str_pad($step, 2, '0', STR_PAD_LEFT).':15</option>';
			$vagtHours .= '<option value="'.str_pad($step, 2, '0', STR_PAD_LEFT).':30:00">'.str_pad($step, 2, '0', STR_PAD_LEFT).':30</option>';
			$vagtHours .= '<option value="'.str_pad($step, 2, '0', STR_PAD_LEFT).':45:00">'.str_pad($step, 2, '0', STR_PAD_LEFT).':45</option>';
		}

		$timeTyper = \ORM::for_table('project_hour_types')
			->where('project_id', $this->projectId)
			->where('active', 1)
			->order_by_desc('pre_select')
			->order_by_asc('sorting')
			->find_array();

		$hourTypeOptions = '';
		foreach ($timeTyper as $timeTyperRow)
		{
			$hourTypeOptions .= '<option value="'.$timeTyperRow['id'].'">'.$timeTyperRow['name'].'</option>';
		}

		$main .= '<div id="workHourModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<h4 class="modal-title">Vagt oprettelse / redigering</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  </div>
			  <div class="modal-body">
				<table class="table">
					<tr id="kopiRow">
						<td>Kopi fra dato</td>
						<td><input type="date" id="kopiFraDato"></td>
					</tr>
					<tr class="vagtRow">
						<td>Vagt start dato</td>
						<td id="start_date" data-value="2019-09-22">22-09-2019</td>
					</tr>
					<tr class="vagtRow">
						<td>Vagt start tidspunkt</td>
						<td><select class="form-control timeSelect" name="startTime" id="startTime">'.$vagtHours.'</select></td>
					</tr>
					<tr class="vagtRow">
						<td>Vagt slut dato</td>
						<td id="end_date" data-value="2019-09-22">22-09-2019</td>
					</tr>
					<tr class="vagtRow">
						<td>Vagt slut</td>
						<td><select class="form-control timeSelect" name="endTime" id="endTime">'.$vagtHours.'</select></td>
					</tr>
					<tr class="vagtRow">
						<td>Type</td>
						<td><select class="form-control" name="hourTypeId" id="hourTypeId">'.$hourTypeOptions.'</select></td>
					</tr>
					<input type="hidden" value="" id="fromId">
				</table>
			  </div>
			  <div class="modal-footer">
			  <div class="row" style="width: 100%;">
			  	<div class="col-2">
			  		<button type="button" class="btn btn-danger" id="deleteWorkHours">Slet</button>
				</div>
				<div class="col-6"></div>
			  	<div class="col-2">
					<button type="button" class="btn btn-success" id="saveWorkHours">Gem</button>
				</div>
				<div class="col-2">
					<button type="button" class="btn btn-info" data-dismiss="modal">Luk</button>
				</div>
			</div>
				
			  </div>
			</div>
		
		  </div>
		</div>';

		$this->jsFiles[] = 'mainPage_1.js';

		return $main;
	}

	public function getTotals()
	{
		$totals = '';

		$totals .= '<table class="table table-bordered">';
		$typeTotals = \ORM::for_table('project_hour_types')
			->where('project_hour_types.project_id', $this->projectId)
			->join('user_work_hours', 'user_work_hours.hour_type_id = project_hour_types.id')
            ->where('user_work_hours.active', 1)
			->where('user_work_hours.periode_id', $this->periodeId)
			->where('user_work_hours.user_id', $this->userId())
			->group_by('project_hour_types.number')
			->select('project_hour_types.name')
			->select_expr('SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60)', 'hours');

		$typeTotals = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('user_work_hours.periode_id', $this->periodeId)
			->where('user_work_hours.user_id', $this->userId())
			->join('project_hour_types', 'user_work_hours.hour_type_id = project_hour_types.id')
			->select('project_hour_types.name')
			->group_by('project_hour_types.number')
			->select_expr('SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60)', 'hours');


		foreach ($typeTotals->find_array() as $typeTotalsRow)
		{
			$totals .= '<tr><td>'.$typeTotalsRow['name'].'</td><td align="right">'.number_format($typeTotalsRow['hours'], 2).'</td></tr>';
		}

		$totals .= '</table>';
		return $totals;
	}

	public function getApprovel()
	{
		$totals = '';

		$totals .= '<table class="table table-bordered">';
		$typeTotals = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('user_work_hours.periode_id', $this->periodeId)
			->where('user_work_hours.project_id', $this->projectId)
			->where('user_work_hours.user_id', $this->userId())
			->group_by('user_approved_user_id')
			->group_by('project_approved_user_id')
			->select_expr('(CASE WHEN user_approved_at IS NOT NULL AND project_approved_at IS NULL THEN "Medarbejder godkendt" WHEN project_approved_at IS NOT NULL THEN "Borger godkendt" ELSE "Ikke godkendt" END)', 'status')
			->select_expr('SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60)', 'hours');

		$totalHours = 0;

		foreach ($typeTotals->find_array() as $typeTotalsRow)
		{
			//dump(\ORM::get_last_statement());
			$totalHours = $totalHours + $typeTotalsRow['hours'];
			$totals .= '<tr><td align="left">'.$typeTotalsRow['status'].'</td><td align="right">'.number_format($typeTotalsRow['hours'], 2).'</td></tr>';
			if($typeTotalsRow['status'] == 'Ikke godkendt' and (!$this->periodeStatus or $this->container['userToken']->returnTo))
			{
				$approvel = '<a class="btn btn-success" style="color: #FFFFFF; cursor: pointer;" id="approvHours">Godkend timer</a>';
			}
		}

		$totals .= '<tr style="font-weight: bold;"><td align="left">Total i perioden</td><td align="right">'.number_format($totalHours, 2).'</td></tr>';
		$totals .= '</table>';
		$totals .=	$approvel;
		return $totals;
	}



	public function projectPage()
	{
		$content = '';

		$workers = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('project_id', $this->projectId)
			->where('periode_id', $this->periodeId)
			->join('user', 'user.id = user_work_hours.user_id')
			->select('user.*')
			->group_by('user.id')
			->find_array();

		$hours = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('project_id', $this->projectId)
			->where('periode_id', $this->periodeId)
			->select_expr('SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60)', 'hours')
			->select_expr('DATE(start_date)', 'date')
			->select('user_id')
			->where_not_null('user_work_hours.user_approved_at')
			->group_by_expr('DATE(start_date)')
			->group_by('user_id')
			->find_array();

		$hoursMaster = [];
		foreach ($hours as $hourRow)
		{
			$hoursMaster[$hourRow['date']][$hourRow['user_id']] = $hourRow['hours'];
		}

		$openForApproval = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->where('project_id', $this->projectId)
			->where('periode_id', $this->periodeId)
			->select_expr('SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60)', 'hours')
			->select('user_id')
			->where_not_null('user_work_hours.user_approved_at')
			->where_null('project_approved_at')
			->group_by('user_id')
			->find_array();
		$userForApproval = [];
		foreach ($openForApproval as $openForApprovalRow)
		{
			$userForApproval[$openForApprovalRow['user_id']] = $openForApprovalRow['hours'];
		}

		$dates = [];
		$runDate = $this->periodeStart;

		for($step = 1; $step < 35; $step ++)
		{
			$dates[] = $runDate;

			if($runDate == $this->periodeEnd)
			{
				break;
			}

			$runDate = date('Y-m-d', strtotime('+ 1 day', strtotime($runDate)));
		}

		$content .= '<table class="table table-bordered" style="font-size: 11px;">';
		$content .= '<tr>';
		$content .= '<td width="100">Datoer</td>';
		foreach ($workers as $workerRow)
		{
			$content .= '<td width="110">'.$workerRow['name'].' ('.$workerRow['user_number'].')</td>';
		}
		$content .= '<td></td>';
		$content .= '<td width="70">Total</td>';
		$content .= '</tr>';

		$dayTotal = [];
		$userTotal = [];
		$projectTotal = 0;

		foreach ($dates as $dateRow)
		{
			$content .= '<tr>';
			$content .= '<td>'.date('d-m-Y', strtotime($dateRow)).'</td>';

			foreach ($workers as $workerRow)
			{
				$content .= '<td align="center" class="hourInfo" style="cursor: pointer;" workerId="'.$workerRow['id'].'">'.$this->showHours($hoursMaster[$dateRow][$workerRow['id']]).'</td>';
				$dayTotal[$dateRow] = $dayTotal[$dateRow] + $hoursMaster[$dateRow][$workerRow['id']];
				$userTotal[$workerRow['id']] = $userTotal[$workerRow['id']] + $hoursMaster[$dateRow][$workerRow['id']];
				$projectTotal = $projectTotal + $hoursMaster[$dateRow][$workerRow['id']];
			}

			$content .= '<td></td>';
			$content .= '<td width="70" align="center" style="font-weight: bold;">'.$this->showHours($dayTotal[$dateRow]).'</td>';
			$content .= '</tr>';
		}

		$content .= '<tr style="font-weight: bold;">';
		$content .= '<td>Totaler</td>';
		foreach ($workers as $workerRow)
		{
			$content .= '<td align="center">'.$userTotal[$workerRow['id']].'</td>';
		}
		$content .= '<td></td>';
		$content .= '<td align="center">'.$this->showHours($projectTotal).'</td>';
		$content .= '</tr>';

		$content .= '<tr style="font-weight: bold;">';
		$content .= '<td></td>';
		$approveAll = null;
		foreach ($workers as $workerRow)
		{
			$content .= '<td align="center">';

			if($userTotal[$workerRow['id']] > 0 AND $userForApproval[$workerRow['id']] > 0 AND (!$this->periodeStatus or $this->container['userToken']->returnTo))
			{
				$content .= '<a class="btn btn-success approvWorker" workerId="'.$workerRow['id'].'" style="color: #FFFFFF; cursor: pointer;">Godkend</a>';
				$approveAll = true;
			}

            if($userTotal[$workerRow['id']] > 0 AND empty($userForApproval[$workerRow['id']]) AND (!$this->periodeStatus or $this->container['userToken']->returnTo))
            {
                $content .= '<a class="btn btn-danger removeApprovWorker" workerId="'.$workerRow['id'].'" style="color: #FFFFFF; font-size: 8px; cursor: pointer;">Fjern godkendelse</a>';
                $approveAll = true;
            }
			$content .= '</td>';
		}
		$content .= '<td></td>';
		$content .= '<td align="center"></td>';
		$content .= '</tr>';

		$content .= '</table>';

		$content .= '<table style="text-align: center; width: 100%;">';

		if($approveAll)
		{
			$content .= '<tr><td><a class="btn btn-success approvWorker" workerid="all" style="color: #FFFFFF; cursor: pointer;">Godkend ALLE</a></td>';
		}

		$content .= '<td><a href="/report" target="_report" class="btn btn-info" style="color: #FFFFFF; cursor: pointer;">Åben rapport</a></td>';

		if($this->periodeStatus != 1)
		{
			$content .= '<td><a class="btn btn-danger removeApproval" workerid="all" style="color: #FFFFFF; cursor: pointer;">Fjern godkendelse</a></td>';
		}

		$content .= '</tr></table>';

		$content .= '<br><hr><h3>Vagt type fordeling</h3>';

		$content .= '<table class="table">';
		$content .= '<thead>';
		$content .= '<tr>';
		$content .= '<th style="width: 150px;">Vagt type nummer</th>';
		$content .= '<th>Vagt type</th>';
		$content .= '<th style="width: 120px;">Antal timer</th>';
		$content .= '</tr>';
		$content .= '</thead>';
		$content .= '<tbody>';

		$hourTypeList = \ORM::for_table('project_hour_types')
            ->join('user_work_hours', 'user_work_hours.hour_type_id = project_hour_types.id AND user_work_hours.active = 1')
            ->where('user_work_hours.periode_id', $this->periodeId)
            ->where('user_work_hours.project_id', $this->projectId)
            ->select('project_hour_types.number')
            ->select('project_hour_types.name')
            ->select_expr('SUM((unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60)', 'hours')
            ->group_by('project_hour_types.id')
            ->find_array();

		foreach ($hourTypeList as $hourTypeListRow)
        {
            $content .= '<tr>';
            $content .= '<td>'.$hourTypeListRow['number'].'</td>';
            $content .= '<td>'.$hourTypeListRow['name'].'</td>';
            $content .= '<td align="right">'.number_format($hourTypeListRow['hours'], 2, ',', '.').'</td>';
            $content .= '</tr>';
        }

		$content .= '</tbody>';
		$content .= '</table>';


		$this->jsFiles[] = 'mainPage_2.js';
		return $content;
	}

	public function showHours($hours)
	{
		if($hours > 0)
		{
			return number_format($hours, 2);
		}
	}

	public function approveReport(Request $request, Response $response, $args)
	{
		$pdf = new \TCPDF('L');

		// set document information
		$pdf->SetCreator('Focus People');
		$pdf->SetAuthor('Focus People');
		$pdf->SetTitle('Raport 1');

		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins('10', '10', '10');

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		$pdf->Image('../public/images/focus_stor.png', 10, 10, 80, '', 'PNG', 'http://www.focus-people.dk', '', true, 150, '', false, false, 0, false, false, false);

		$pdf->SetXY(10,10);
		$pdf->Cell(190,'','Udskrivet d. '.date('d-m-Y H:i:s'),0,0,'R');

		$pdf->SetXY(10,30);
		$pdf->Cell(190,'','Projekt ID: '.$this->projectNumber ,0,0,'L');
		$pdf->Ln();
		$pdf->Cell(190,'','Projekt Navn: '.$this->userName() ,0,0,'L');

		$pdf->SetXY(10,40);
		$pdf->Cell(60,'','Medarbejder','LRT',0,'L');
		$pdf->Cell(20,'','Start','LRT',0,'L');
		$pdf->Cell(20,'','Start','LRT',0,'L');
		$pdf->Cell(20,'','Slut','LRT',0,'L');
		$pdf->Cell(20,'','Slut','LRT',0,'L');
		$pdf->Cell(30,'','M. Godkendt','LRT',0,'L');
		$pdf->Cell(30,'','B. Godkendt','LRT',0,'L');
		$pdf->Cell(20,'','Timer','LRT',0,'C');
		$pdf->Cell(35,'','Art','LRT',0,'L');
		$pdf->Ln();

		$pdf->SetFont('helvetica', '', 8);

		$hours = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.active', 1)
			->left_outer_join('user', 'user.id = user_work_hours.user_id')
			->left_outer_join('project_hour_types', 'project_hour_types.id = user_work_hours.hour_type_id')
			->select('user.name', 'user_name')
			->select('user.user_number', 'user_number')
			->select('project_hour_types.name', 'type_name')
			->select('user_work_hours.*')
			->select_expr('(unix_timestamp(user_work_hours.end_date) - unix_timestamp(user_work_hours.start_date))/60/60', 'hours')
			->order_by_asc('user_work_hours.start_date')
			->where('user_work_hours.project_id', $this->projectId)
			->where('user_work_hours.periode_id', $this->periodeId)
			->find_array();

		foreach ($hours as $hourRow)
		{
			$pdf->Cell(60,'',$hourRow['user_name'].' ('.$hourRow['user_number'].')','LRT',0,'L');
			$pdf->Cell(20,'',date('d-m-Y', strtotime($hourRow['start_date'])),'LRT',0,'L');
			$pdf->Cell(20,'',date('H:i', strtotime($hourRow['start_date'])),'LRT',0,'L');
			$pdf->Cell(20,'',date('d-m-Y', strtotime($hourRow['end_date'])),'LRT',0,'L');
			$pdf->Cell(20,'',date('H:i', strtotime($hourRow['end_date'])),'LRT',0,'L');
			$pdf->Cell(30,'', $this->showApproveDate($hourRow['user_approved_at']),'LRT',0,'L');
			$pdf->Cell(30,'', $this->showApproveDate($hourRow['project_approved_at']),'LRT',0,'L');
			$pdf->Cell(20,'',$this->showHours($hourRow['hours']),'LRT',0,'C');

			$pdf->Cell(35,'',$hourRow['type_name'],'LRT',0,'L');
			$pdf->Ln();
		}

		$pdf->Cell(255,'','','T',0,'L');

		// ---------------------------------------------------------

		//Close and output PDF document
		//$pdf->Output('Raport_.pdf', 'I');

		$response->withHeader( 'Content-type', 'application/pdf' );
		$response->write( $pdf->Output('My cool PDF', 'I') );
		die();
		//return $response;

	}

	public function showApproveDate($date)
	{
		if(is_null($date))
		{
			return '';
		}

		return date('d-m-Y H:i',strtotime($date));
	}

	public function adminPage()
	{
		$userTypes[1] = 'Medarbejder';
		$userTypes[2] = 'Borger';

		$main = '';

		$main .= '<form name="sogForm" method="get" action="">';
		$main .= '<table class="table table-bordered">';
		$main .= '<tr>';
		$main .= '<td>Søgning</td>';
		$main .= '<td><input class="form-control" placeholder="Søg efter nummer, navn eller mobil nummer" value="'.$_GET['s'].'" name="s"></td>';
		$main .= '<td><button class="btn btn-success" name="sog" value="sog">Søg</button></td>';
		$main .= '<td><a class="btn btn-info" href="/">Ryd</a></td>';
		$main .= '</tr>';
		$main .= '</table>';
		$main .= '</form>';

		$main .= '<table class="table table-striped table-bordered" id="userTable">';
		$main .= '<thead>';
		$main .= '<tr>';
		$main .= '<th>Type</th>';
		$main .= '<th>Nr</th>';
		$main .= '<th>Navn</th>';
		$main .= '<th>Mobil nummer</th>';
		$main .= '<th></th>';
		$main .= '<th></th>';
		$main .= '<th></th>';
		$main .= '</tr>';
		$main .= '</thead>';
		$main .= '<tbody>';
		$users = \ORM::for_table('user');
            //->where('active', 1);
			//->where_null('internal_user');

		if(!empty($_GET['s']))
		{
			$users->where_raw('
			(
			user_number like CONCAT("%",?,"%")
			OR
			name like CONCAT("%",?,"%")
			OR
			cell_number like CONCAT("%",?,"%")
			)
			', [$_GET['s'], $_GET['s'], $_GET['s']]);
		}

		foreach ($users->find_array() as $userRow)
		{
			$listButton = '';
			if($userRow['user_type'] == 1)
			{
				$listButton = '<button class="btn btn-primary fullList" name="'.$userRow['name'].'" userId="'.$userRow['id'].'"><i class="fa fa-list"></i></button>';
			}

			$main .= '<tr>';
			$main .= '<td>'.$userTypes[$userRow['user_type']].'</td>';
			$main .= '<td>'.$userRow['user_number'].'</td>';
			$main .= '<td>'.$userRow['name'].'</td>';
			$main .= '<td>'.$userRow['cell_number'].'</td>';
			$main .= '<td align="center">'.$listButton.'</td>';
			$main .= '<td align="center"><button class="btn btn-danger logInAs" userId="'.$userRow['id'].'">Log in</button></td>';
			$main .= '<td align="center"><button class="btn btn-success newKode" userId="'.$userRow['id'].'">Kode</button></td>';
			$main .= '</tr>';
		}
		$main .= '</tbody>';
		$main .= '</table>';

		$this->jsFiles[] = 'mainAdminPage.js';
		return $main;
	}

	public function perioder(Request $request, Response $response, $args)
	{
		$main = '<table class="table table-bordered">';

		foreach (\ORM::for_table('periodes')->order_by_asc('start_date')->find_array() as $periodeRow)
		{
			$main .= '<tr>';
			$main .= '<td>'.$periodeRow['start_date'].'</td>';
			$main .= '<td>'.$periodeRow['end_date'].'</td>';
			if($periodeRow['locked'] == 1)
			{
				$main .= '<td class="table-danger">Låst</td>';
			}
			else
			{
				$main .= '<td><a class="btn btn-danger lockPeriod" style="color: #FFFFFF;" pid="'.$periodeRow['id'].'">Lås</a></td>';
			}
			$main .= '</tr>';
		}

		$main .= '</table>';

		$this->jsFiles[] = 'mainAdminPeriode.js';
		$this->displayPage($main);

	}

    public function mobile(Request $request, Response $response, $args)
    {
        $this->mobile = true;

        $dates = [];
        $runDate = strtotime($this->periodeStart);
        for($step = 1; $step < 35; $step ++)
        {
            $dates[] = date('d-m-Y', $runDate);

            if(date('Y-m-d', $runDate) == $this->periodeEnd)
            {
                break;
            }

            $runDate = $runDate + (60*60*24);
        }

        $displayDateSelectValues = '';
        foreach ($dates as $dateRow)
        {
            $displayDateSelectValues .= '<option value="'.date('Y-m-d', strtotime($dateRow)).'">'.$dateRow.'</option>';
        }

        $vagtHours = '';
        for ($step = 0; $step < 24; $step ++)
        {
            $vagtHours .= '<option value="'.str_pad($step, 2, '0', STR_PAD_LEFT).':00:00">'.str_pad($step, 2, '0', STR_PAD_LEFT).':00</option>';
            $vagtHours .= '<option value="'.str_pad($step, 2, '0', STR_PAD_LEFT).':15:00">'.str_pad($step, 2, '0', STR_PAD_LEFT).':15</option>';
            $vagtHours .= '<option value="'.str_pad($step, 2, '0', STR_PAD_LEFT).':30:00">'.str_pad($step, 2, '0', STR_PAD_LEFT).':30</option>';
            $vagtHours .= '<option value="'.str_pad($step, 2, '0', STR_PAD_LEFT).':45:00">'.str_pad($step, 2, '0', STR_PAD_LEFT).':45</option>';
        }

        $timeTyper = \ORM::for_table('project_hour_types')
            ->where('project_id', $this->projectId)
            ->where('active', 1)
            ->order_by_desc('pre_select')
            ->order_by_asc('sorting')
            ->find_array();

        $hourTypeOptions = '';
        foreach ($timeTyper as $timeTyperRow)
        {
            $hourTypeOptions .= '<option value="'.$timeTyperRow['id'].'">'.$timeTyperRow['name'].'</option>';
        }

        $main = '<h5>Opret ny</h5>';
        $main .= '
        <table class="table">
					<tr class="vagtRow">
						<td>Start dato</td>
						<td><select class="form-control timeSelect" name="startDate" id="startDate">'.$displayDateSelectValues.'</select></td>
					</tr>
					<tr class="vagtRow">
						<td>Vagt start tidspunkt</td>
						<td><select class="form-control timeSelect" name="startTime" id="startTime">'.$vagtHours.'</select></td>
					</tr>
					<tr class="vagtRow">
						<td>Vagt slut dato</td>
						<td><select class="form-control timeSelect" name="endDate" id="endDate">'.$displayDateSelectValues.'</select></td>
					</tr>
					<tr class="vagtRow">
						<td>Vagt slut</td>
						<td><select class="form-control timeSelect" name="endTime" id="endTime">'.$vagtHours.'</select></td>
					</tr>
					<tr class="vagtRow">
						<td>Type</td>
						<td><select class="form-control" name="hourTypeId" id="hourTypeId">'.$hourTypeOptions.'</select></td>
					</tr>
					<tr id="showHours" style="display: none;">
					    <td>Antal timer</td>
					    <td align="right" id="showHourNumber"></td>
                    </tr>
					<tr id="saveRow" style="display: none;">
					    <td><a href="/mobile" class="btn btn-info">Anullere</a></td>
					    <td align="right"><a class="btn btn-success active" id="saveNow">Gem</a></td>
                    </tr>
				</table>
        ';


        $main .= '<h5>Liste</h5>';
        $main .= '<table class="table" style="font-size: smaller;">';
        $main .= '<thead>';
        $main .= '<tr>';
        $main .= '<th>Start</th>';
        $main .= '<th>Start</th>';
        $main .= '<th>Start</th>';
        $main .= '<th></th>';
        $main .= '</tr>';
        $main .= '</thead>';


        $openList = \ORM::for_table('user_work_hours')
            ->where('user_work_hours.periode_id', $this->periodeId)
            ->where('user_work_hours.project_id', $this->projectId)
            ->where('user_work_hours.user_id', $this->userId())
            ->where('user_work_hours.active', 1)
            ->left_outer_join('project_hour_types', 'project_hour_types.id = user_work_hours.hour_type_id')
            ->select('user_work_hours.*')
            ->select('project_hour_types.name')
            ->order_by_asc('user_work_hours.start_date');

        foreach ($openList->find_array() as $listRow)
        {
            $main .= '<tr>';

            $main .= '<td>'.date('d-m | H:i', strtotime($listRow['start_date'])).'</td>';
            $main .= '<td>'.date('d-m | H:i', strtotime($listRow['end_date'])).'</td>';
            $main .= '<td>'.substr($listRow['name'],0,3).'</td>';

            $sletKnap = '';
            if(is_null($listRow['user_approved_at']))
            {
                $sletKnap = '<a class="btn btn-warning active btn-sm mobileDelete" workHourId="'.$listRow['id'].'" style="height: 20px; font-size: 8px; color: #FFFFFF;">Slet</a>';
                $sletKnap .= '<a style="display: none;" class="btn btn-danger active btn-sm mobileDeleteConfirm" workHourId="'.$listRow['id'].'">Slet Nu!</a>';
            }

            $main .= '<td>'.$sletKnap.'</td>';

            $main .= '</tr>';
        }

        $main .= '</table>';

        $this->jsFiles[] = 'mobile.js';
        $this->displayPage($main);
    }

}