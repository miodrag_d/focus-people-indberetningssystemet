<?php


namespace app\app\pages;
use app\app\controllers\mainController;
use Slim\Http\Request;
use Slim\Http\Response;

class perioder extends mainController
{
	public function periodeShift(Request $request, Response $response, $args)
	{
		$content = '';

		$content .= '<table class="table table-bordered">';
		$content .= '<thead>';
		$content .= '<tr>';
		$content .= '<th>Periode</th>';
		$content .= '<th>Status</th>';
		$content .= '<th></th>';
		$content .= '</tr>';
		$content .= '</thead>';
		foreach (\ORM::for_table('periodes')->order_by_desc('end_date')->find_array() as $perRow)
		{
			$status = '';

			if($perRow['locked'])
			{
				$status = '<i class="fa fa-lock"></i>';
			}

			$content .= '<tr>';
			$content .= '<td>'.date('d-m-Y', strtotime($perRow['start_date'])).' - '.date('d-m-Y', strtotime($perRow['end_date'])).'</td>';
			$content .= '<td align="center">'.$status.'</td>';
			$content .= '<td align="center"><a class="btn btn-success shiftPeriod" pid="'.$perRow['id'].'" style="color: #FFFFFF;">Skift</a></td>';
			$content .= '</tr>';
		}

		$content .= '</table>';

		return $response->withJson(['content' => $content]);
	}

	public function setPerioed(Request $request, Response $response, $args)
	{
		$periode = \ORM::for_table('periodes')
			->where('id', $args['id'])
			->find_one();

		$this->setFocusC($periode->as_array());
	}
}