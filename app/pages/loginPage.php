<?php


namespace app\app\pages;
use app\app\controllers\mainController;
use app\app\controllers\smsSender;
use Firebase\JWT\JWT;
use League\Plates\Engine;
use Slim\Http\Request;
use Slim\Http\Response;

class loginPage extends mainController
{
	public function index(Request $request, Response $response, $args)
	{
		$template = new Engine(DIR_SRC.'templates');

		echo $template->render('login.html');
	}

	public function login(Request $request, Response $response, $args)
	{
		$token = '';
		$message = '';
		$postBody = $request->getParsedBody();
		$this->logger->debug('login', [$postBody]);

		if(!empty($postBody['user']) AND !empty($postBody['password']))
		{
			//find the user
			$user = \ORM::for_table('user')
				->where('user_number', $postBody['user'])
				->where_raw('password = PASSWORD(?)', [$postBody['password']])
				->find_one();

			if($user)
			{
				$projects = \ORM::for_table('projects')
					->join('user_project', 'user_project.project_id = projects.id')
					->where('user_project.active', 1)
					->select('projects.id')
					->select('projects.number')
					->select('projects.name')
					->where('user_project.user_id', $user['id'])
					->find_array();

				$token = JWT::encode([
					'user' => $user['id'],
					'name' => $user['name'],
					'user_type' => $user['user_type'],
					'projects' => json_encode($projects)
				], $_ENV['KEY']);
			}
			else
			{
				$message = 'Vi kan ikke finde dig i vores system. Prøv igen';
			}
		}
		else
		{
			$message = 'Der er felter du mangler at udfylde';
		}

		return $response->withJSON(['message' => $message, 'token' => $token]);
	}

	public function logout(Request $request, Response $response, $args)
	{
		setcookie("focusPeople", NULL, 0, "/");
		unset($_SESSION['periode']);
		return $response->withRedirect('/login');
	}

	public function forgotPage()
	{
		$template = new Engine(DIR_SRC.'templates');

		echo $template->render('forgotPassword.html');
	}

	public function newPassword(Request $request, Response $response, $args)
	{
		$postData = $request->getParsedBody();

		if(strlen($postData['cellNumber']) != 8)
		{
			return $response->withJson(['message' => 'Mobil nummer er for kort', 'type' => 'bad']);
		}

		$user = \ORM::for_table('user')
			->where('cell_number', $postData['cellNumber'])
			->find_one();

		$userActive = \ORM::for_table('user_project')
			->where('user_id', $user['id'])
			->where('active', 1)
			->find_one();

		if(!$user OR !$userActive)
		{
			return $response->withJson(['message' => 'Kan desværre ikke finde dig. Prøv igen', 'type' => 'bad']);
		}

		$newPassword = rand(10000,9999999);
		$user->set_expr('password', 'PASSWORD('.$newPassword.')');
		$user->save();

		//afsendelse af SMS
		$sms = new smsSender();
		$sms->sendSMS($postData['cellNumber'], 'Hej '.$user['name'].'. Dit medarbejder ID/nummer er: '.$user['user_number'].' og din kode er: '.$newPassword.' . Hilsen Focus People');

		return $response->withJson(['message' => 'Besked er sendt via SMS', 'type' => true]);
	}

	public function token(Request $request, Response $response, $args)
	{
		$postData = $request->getParsedBody();

		if($postData['user'] == $_ENV['DATA_USER'] AND $postData['password'] == $_ENV['DATA_PASSWORD'])
		{
			return JWT::encode([
				'user' => $_ENV['DATA_USER'],
				'password' => $_ENV['DATA_PASSWORD'],
				'ip' => $request->getServerParam('REMOTE_ADDR'),
				'exp' => (time() + (60 * 60))
			], $_ENV['KEY']);
		}
	}
}