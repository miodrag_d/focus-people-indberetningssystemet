<?php


namespace app\app\pages;


use app\app\controllers\mainController;
use Slim\Http\Request;
use Slim\Http\Response;

class project extends mainController
{
	public function projectShift(Request $request, Response $response, $args)
	{
		$content = '';

		$content .= '<table class="table table-bordered">';
		$content .= '<thead>';
		$content .= '<tr>';
		$content .= '<th>Projekt</th>';
		$content .= '<th></th>';
		$content .= '</tr>';
		$content .= '</thead>';


		foreach ($this->projects as $pKey => $proRow)
		{
			$status = '<a class="btn btn-success skiftProjectNow" projectIndex="'.$pKey.'" style="color: #FFFFFF;">Skift</a>';

			if($_SESSION['projektSelect'] == $pKey)
			{
				$status = 'Valgt';
			}

			$content .= '<tr>';
			$content .= '<td>'.$proRow['name'].'</td>';
			$content .= '<td align="center">'.$status.'</td>';
			$content .= '</tr>';
		}

		$content .= '</table>';

		return $response->withJson(['content' => $content]);
	}

	public function setProjectIndex(Request $request, Response $response, $args)
	{
		$_SESSION['projektSelect'] = $args['id'];

	}
}