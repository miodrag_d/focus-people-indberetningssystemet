<?php


use app\app\controllers\adminPages;
use app\app\controllers\auth;
use app\app\controllers\apiData;
use app\app\controllers\dataExport;
use app\app\controllers\editorApi;
use app\app\controllers\fixer;
use app\app\pages\loginPage;
use app\app\pages\mainPage;
use app\app\pages\perioder;
use app\app\pages\project;

$app->get('/login', loginPage::class . ':index');
$app->get('/fg', loginPage::class . ':forgotPage');

$auth = new auth($app);

$app->group('/auth', function (){
	$this->post('/login', loginPage::class . ':login');
	$this->post('/newPassword', loginPage::class . ':newPassword');
	$this->post('/token', loginPage::class . ':token');
});

$app->get('/', mainPage::class . ':index')->add($auth);
$app->get('/mobile', mainPage::class . ':mobile')->add($auth);
$app->get('/perioder', mainPage::class . ':perioder')->add($auth);
$app->get('/logout', loginPage::class . ':logout')->add($auth);
$app->get('/report', mainPage::class . ':approveReport')->add($auth);
//$app->get('/fixer/projects', fixer::class . ':fixProjects')->add($auth);
//$app->get('/fixer/sommertime', fixer::class . ':sommerTime')->add($auth);
$app->get('/fixer/findPeriodeErrors', fixer::class . ':findPeriodeErrors')->add($auth);

$app->group('/admin', function (){
    $this->get('/rapportlist', adminPages::class . ':rapportList');
    $this->get('/totalreport', adminPages::class . ':totalReport');
    $this->get('/users', adminPages::class . ':superUsers');
    $this->get('/editor', adminPages::class . ':editor');
})->add($auth);

$app->group('/api', function (){
	$this->post('/saveWorkHours', apiData::class . ':saveHours');
	$this->get('/getWorkHours/{id}', apiData::class . ':getWorkHours');
	$this->get('/lastWorkHourType', apiData::class . ':lastWorkHourType');
	$this->get('/deleteWorkHour/{id}', apiData::class . ':deleteWorkHour');
	$this->get('/approvHours', apiData::class . ':approvHours');
	$this->get('/periodeShift', perioder::class . ':periodeShift');
	$this->get('/shift/{id}', perioder::class . ':setPerioed');
	$this->get('/projectShift', project::class . ':projectShift');
	$this->get('/shiftProject/{id}', project::class . ':setProjectIndex');
	$this->get('/approveWorker/{id}', apiData::class . ':approveWorker');
	$this->get('/workerInfo/{id}', apiData::class . ':workerInfo');
	$this->get('/getUserToken/{id}', apiData::class . ':getUserToken');
	$this->post('/resetpassword', apiData::class . ':resetPassword');
	$this->get('/copyWorkDateList/{date}', apiData::class . ':copyDateList');
	$this->get('/removeapproval', apiData::class . ':removeApproval');
	$this->get('/removeapprovalworker/{workerId}', apiData::class . ':removeApprovalWorker');
	$this->get('/fullList/{id}', apiData::class . ':fullList');
	$this->get('/lookperiode/{id}', apiData::class . ':lookPeriode');
    $this->post('/deleteManyWorkHours', apiData::class . ':deleteManyWorkHours');
    $this->post('/hourCheck', apiData::class . ':hourCheck');
    $this->get('/savesuperuser/{userId}/{status}', apiData::class . ':saveSuperUser');
    $this->post('/getEditorList', apiData::class . ':getEditorList');
})->add($auth);

$app->group('/editorapi', function (){
    $this->post('/actionHandler', editorApi::class . ':actionHandler');
    $this->get('/getHourTypes/{rowId}', editorApi::class . ':getHourTypes');
    $this->post('/checkHourEdit', editorApi::class . ':checkHourEdit');
    $this->get('/workHourRowLog/{rowId}', editorApi::class . ':workHourRowLog');
})->add($auth);

$apiauth = new \app\app\controllers\apiauth($app);

/*
$app->group('/data', function (){
	$this->get('/status', dataExport::class . ':getStatus');
	$this->get('/get', dataExport::class . ':dataExport');
})->add($apiauth);
*/

$app->group('/data', function (){
	$this->get('/status', dataExport::class . ':getStatus');
	$this->get('/get', dataExport::class . ':dataExport');
	$this->get('/over24', dataExport::class . ':workManyHours');
});